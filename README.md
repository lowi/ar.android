# Android.ar

Experiments in augmented reality.
At the moment it is unclear what is supposed to become. 

However, the foundations have been laid.

*The architecture is pretty decent (if I say so myself…)*

## Architecture

### Modules

The Application is split into several modules:

- app: The actual application
- architecture-core: the mvvm-backbone
- data: everything IO (except screen)
- domain: domain specific data-structures
- gl: everything openGL 

#### app

The main presentation module. 
It contains the application, the screen-fragments and their respective viewmodels & state.  

#### architecture-core

The architectural scaffold. Taken from [https://github.com/qingmei2/MVVM-Architecture](https://github.com/qingmei2/MVVM-Architecture)
and adapted to my needs. The principle architecture is [clean](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) 
and uses [MVVM](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) for the UI.

The core extends Fragments, ViewModels, Activities and such. It provides interfaces for stuff like 
repositories and state. And it provides a bunch of extension making life with [jetpack](https://developer.android.com/jetpack) 
more enjoyable in general.

#### data

Here we have everything IO. Loading assets. Talking to the network. The likes. 
Everything with sid-effects that does not involve the screen goes here.
 
#### domain

Domain specific data-structures, eg. entities, repository-interfaces, errors… 

#### gl
 
Everything openGL and augmented reality. Might as well be in the app-module. But I wanted to isolate
it as much as possible


### AR




### Navigation

We're using jetpack-navigation because it is halfway sane.


### Dependency injection

We're using Koin for dependency-injection. 

### Arrow FX

[Arrow FX](https://arrow-kt.io/) is fun. You should try it.
