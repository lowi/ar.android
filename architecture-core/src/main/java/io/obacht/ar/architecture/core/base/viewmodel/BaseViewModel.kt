package io.obacht.ar.architecture.core.base.viewmodel

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel()