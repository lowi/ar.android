package io.obacht.ar.architecture.core.base.repository

interface IRepository

interface IRemoteDataSource

interface ILocalDataSource