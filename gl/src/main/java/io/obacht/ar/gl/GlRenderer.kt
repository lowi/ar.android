package io.obacht.ar.gl

import android.app.Activity
import android.opengl.GLES30.*
import android.opengl.GLSurfaceView
import android.util.Pair
import arrow.core.Option
import arrow.fx.IO
import arrow.fx.extensions.fx
import com.google.ar.core.*
import io.obacht.ar.domain.entity.gl.AssetMap
import io.obacht.ar.domain.entity.gl.Light
import io.obacht.ar.gl.background.CameraRenderer
import io.obacht.ar.gl.scene.SceneParameters
import io.obacht.ar.gl.scene.SimpleBoxScene
import io.obacht.ar.gl.utils.DisplayRotationHelper
import io.obacht.ar.gl.utils.TrackingStateHelper
import java.util.*
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10


class GlRenderer(
    private val cameraRenderer: CameraRenderer
) : GLSurfaceView.Renderer {

    // these are being initialized during .initialize() called by the fragment
    private lateinit var activity: Activity
    private lateinit var session: Session
    private lateinit var assets: AssetMap
    private lateinit var trackingStateHelper: TrackingStateHelper
    private lateinit var displayRotationHelper: DisplayRotationHelper
    private lateinit var messageFn: (String) -> Unit

    private val scene: SimpleBoxScene = SimpleBoxScene()


    // Augmented image and its associated center pose anchor,
    // keyed by index of the augmented image in the database.
    // TODO: put this (local state) into the viewmodel state
    private val augmentedImageMap: MutableMap<Int, Pair<AugmentedImage, Anchor>> = HashMap()


    fun initialize(
        activity_: Activity,
        session_: Session,
        surfaceView_: GLSurfaceView,
        assets_: AssetMap,
        trackingStateHelper_: TrackingStateHelper,
        displayRotationHelper_: DisplayRotationHelper,
        messageFn_: (String) -> Unit,
    ): IO<Unit> = IO.fx {

        // assign members
        activity = activity_
        session = session_
        assets = assets_
        trackingStateHelper = trackingStateHelper_
        displayRotationHelper = displayRotationHelper_
        messageFn = messageFn_

        initGlContext(surfaceView_)
    }

    private fun initGlContext(surfaceView: GLSurfaceView) {
        // setup the openGL context
        surfaceView.preserveEGLContextOnPause = true
        surfaceView.setEGLContextClientVersion(3)
        surfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 8) // Alpha used for plane blending.
        surfaceView.setRenderer(this)
        surfaceView.renderMode = GLSurfaceView.RENDERMODE_CONTINUOUSLY
        surfaceView.setWillNotDraw(false)
    }


    // GLSurfaceView.Renderer functions
    // ————————————————————————————————
    override fun onSurfaceCreated(ignored_gl: GL10, config: EGLConfig) {
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f)
        cameraRenderer.initialize(assets)
        scene.initialize(assets)
    }

    override fun onSurfaceChanged(ignored_gl: GL10, width: Int, height: Int) {
        displayRotationHelper.onSurfaceChanged(width, height)
        glViewport(0, 0, width, height)
    }

    /**
     * onDrawFrame is where the actual rendering happens.
     */
    override fun onDrawFrame(ignored_gl: GL10) {

        glStencilMask(0xff)

        // Clear screen to notify driver it should not load any pixels from previous frame.
        val mask = GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT or GL_STENCIL_BUFFER_BIT
        glClear(mask)

        // Notify ARCore session that the view size changed so that the perspective matrix and
        // the video background can be properly adjusted.
        displayRotationHelper.updateSessionIfNeeded(session)

        // Sets the OpenGL texture handle that will allow GPU access to the camera image.
        session.setCameraTextureName(cameraRenderer.textureId.gl)

        // Obtain the current frame from ARSession. When the configuration is set to
        // UpdateMode.BLOCKING (it is by default), this will throttle the rendering to the
        // camera framerate.
        val frame = session.update()
        val camera = frame.camera

        // Keep the screen unlocked while tracking, but allow it to lock when tracking stops.
        trackingStateHelper.updateKeepScreenOnFlag(camera.trackingState)

        // If frame is ready, render camera preview image to the GL surface.
        cameraRenderer.draw(frame)

        // If not tracking, don't draw 3D objects, show tracking failure reason instead.
        if (camera.trackingState == TrackingState.PAUSED) {
            val message = TrackingStateHelper.getTrackingFailureReasonString(camera)
            messageFn(message)
        } else {

            // Get projection matrix.
            val projectionMatrix = FloatArray(16)
            camera.getProjectionMatrix(projectionMatrix, 0, 0.1f, 100.0f)

            // Get camera matrix.
            val viewMatrix = FloatArray(16)
            camera.getViewMatrix(viewMatrix, 0)

            // Get the light estimate for the current frame.
            val lightEstimate = frame.lightEstimate

            // Get intensity and direction of the main directional light from the current light estimate.
            val intensity = lightEstimate.environmentalHdrMainLightIntensity
            val direction = lightEstimate.environmentalHdrMainLightDirection

            // Get ambient lighting as spherical harmonics coefficients.
            val harmonics = lightEstimate.environmentalHdrAmbientSphericalHarmonics

            // Compute lighting from average intensity of the image.
            // The first three components are color scaling factors.
            // The last one is the average pixel intensity in gamma space.
            val colorCorrectionRgba = FloatArray(4)
            frame.lightEstimate.getColorCorrection(colorCorrectionRgba, 0)

            // Get HDR environmental lighting as a cubemap in linear color space.
            val lightmaps: Array<ArImage> = lightEstimate.acquireEnvironmentalHdrCubeMap()!!

            // prepare the lighting information
            val light = Light(
                intensity, direction, harmonics, lightmaps, colorCorrectionRgba
            )

            // Visualize augmented images.
            val message =
                drawAugmentedImages(frame, projectionMatrix, viewMatrix, light)

            message.fold({}) { messageFn(it) }
        }
    }


    private fun drawAugmentedImages(
        frame: Frame,
        projectionMatrix: FloatArray,
        viewMatrix: FloatArray,
        light: Light
    ): Option<String> {

        val updatedAugmentedImages =
            frame.getUpdatedTrackables(AugmentedImage::class.java)

        var message: Option<String> = Option.empty()

        // Iterate to update augmentedImageMap, remove elements we cannot draw.
        for (augmentedImage in updatedAugmentedImages) {
            when (augmentedImage.trackingState) {

                TrackingState.PAUSED -> {
                    // When an image is in PAUSED state, but the camera is not PAUSED, it has been detected,
                    // but not yet tracked.
                    val text = String.format("Detected Image %d", augmentedImage.index)
                    message = Option.just(text)
                }

                TrackingState.TRACKING -> {

//                    // Have to switch to UI Thread to update View.
//                    this.runOnUiThread(
//                        Runnable { fitToScanView.setVisibility(View.GONE) })

                    // Create a new anchor for newly found images.
                    val centerPoseAnchor = augmentedImage.createAnchor(augmentedImage.centerPose)

                    if (!augmentedImageMap.containsKey(augmentedImage.index)) {
                        augmentedImageMap[augmentedImage.index] =
                            Pair.create(augmentedImage, centerPoseAnchor)
                    }
                }

                // remove images we're not tracking any longer
                TrackingState.STOPPED -> augmentedImageMap.remove(augmentedImage.index)

                // we need this since Java doesn't have no algebraic data-types
                else -> io.obacht.ar.architecture.core.logger.AppLogger.w("unknown tracking state ${augmentedImage.trackingState}")
            }
        }

        // Draw all images in augmentedImageMap
        for (pair in augmentedImageMap.values) {
            val augmentedImage = pair.first
            val centerAnchor: Anchor = augmentedImageMap[augmentedImage.index]!!.second
            val parameters =
                SceneParameters(viewMatrix, projectionMatrix, augmentedImage, centerAnchor, light)
            when (augmentedImage.trackingState) {
                TrackingState.TRACKING -> scene.draw(parameters)
                else -> { /*nada*/
                }
            }
        }
        return message
    }
}