package io.obacht.ar.gl.images

import android.opengl.GLES30
import arrow.fx.IO
import arrow.fx.extensions.fx
import io.obacht.ar.domain.entity.gl.AssetMap
import io.obacht.ar.domain.entity.gl.ShaderSource
import io.obacht.ar.gl.base.GlHandle.*
import io.obacht.ar.gl.base.Shader
import io.obacht.ar.gl.base.compile


// @see: https://learnopengl.com/Advanced-OpenGL/Stencil-testing

class StencilShader(
    program: Program,
    internal val uModelView: Uniform,
    internal val uModelViewProjection: Uniform,
    internal val vPosition: Varying,
) : Shader(program) {


    companion object {

        fun initialize(assets: AssetMap): StencilShader {
            val vertexSource =
                assets["shader_camera_vertex"] as ShaderSource<*>
            val fragmentSource =
                assets["shader_camera_fragment"] as ShaderSource<*>

            val vertexHandle = vertexSource.compile()
            val fragmentHandle = fragmentSource.compile()

            // attach and link the shaders
            val program = Program(GLES30.glCreateProgram())
            GLES30.glAttachShader(program.gl, vertexHandle.gl)
            GLES30.glAttachShader(program.gl, fragmentHandle.gl)
            GLES30.glLinkProgram(program.gl)
            GLES30.glUseProgram(program.gl)

            // shader parameters
            val uModelView = Uniform(GLES30.glGetUniformLocation(program.gl, "u_ModelView"))
            val uModelViewProjection =
                Uniform(GLES30.glGetUniformLocation(program.gl, "u_ModelViewProjection"))
            val vPosition = Varying(GLES30.glGetAttribLocation(program.gl, "a_Position"))
            checkGLError("shader parameters")

            return StencilShader(
                program,
                uModelView,
                uModelViewProjection,
                vPosition
            )
        }
    }
}