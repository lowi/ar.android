package io.obacht.ar.gl.images

import android.opengl.GLES30
import io.obacht.ar.architecture.core.logger.AppLogger
import io.obacht.ar.domain.entity.gl.AssetMap
import io.obacht.ar.domain.entity.gl.ShaderSource
import io.obacht.ar.gl.base.GlHandle
import io.obacht.ar.gl.base.GlHandle.Uniform
import io.obacht.ar.gl.base.GlHandle.Varying
import io.obacht.ar.gl.base.Shader
import io.obacht.ar.gl.base.compile

class CubeShader(

    program: GlHandle.Program,

    // model view projection matrix.
    internal val uModelView: Uniform,
    internal val uModelViewProjection: Uniform,

    // object attributes
    internal val vPosition: Varying,
    internal val vNormal: Varying,

    // Shader location: environment properties.
    internal val uLighting: Uniform,

    // Shader location: material properties.
    internal val uMaterial: Uniform,

    // Shader location: color correction property.
    internal val uColorCorrection: Uniform,

    // Shader location: object color property (to change the primary color of the object).
    internal val uObjColor: Uniform,

    ) : Shader(program) {


    companion object {

        fun initialize(assets: AssetMap): CubeShader  {

            val vertexSource = assets["shader_box_vertex"] as ShaderSource<*>
            val fragmentSource = assets["shader_box_fragment"] as ShaderSource<*>

            val vertexHandle = vertexSource.compile()
            val fragmentHandle = fragmentSource.compile()

            // attach and link the shaders
            val program = GlHandle.Program(GLES30.glCreateProgram())
            GLES30.glAttachShader(program.gl, vertexHandle.gl)
            GLES30.glAttachShader(program.gl, fragmentHandle.gl)
            GLES30.glLinkProgram(program.gl)
            GLES30.glUseProgram(program.gl)


            // shader parameters
            // ————————————————————————

            val uModelView = Uniform(GLES30.glGetUniformLocation(program.gl, "u_ModelView"))

            val uModelViewProjection =
                Uniform(
                    GLES30.glGetUniformLocation(
                        program.gl,
                        "u_ModelViewProjection"
                    )
                )

            val vPosition = Varying(GLES30.glGetAttribLocation(program.gl, "a_Position"))
            val vNormal = Varying(GLES30.glGetAttribLocation(program.gl, "a_Normal"))

            AppLogger.i("create. position handle: ${vPosition.gl}")
            AppLogger.i("create. normal handle: ${vNormal.gl}")

            checkGLError("vertex shader parameters")

            // Fragment shader parameters
            // ——————————————————————————

            // todo: make Uniform and Varying safer by giving their constructors
            //  the gl-context, the program and the name,
            //  instead of calling glGetUniformLocation/glGetAttribLocation beforehand

            val uLighting =
                Uniform(GLES30.glGetUniformLocation(program.gl, "u_Lighting"))

            val uMaterial =
                Uniform(GLES30.glGetUniformLocation(program.gl, "u_Material"))

            val uColorCorrection =
                Uniform(GLES30.glGetUniformLocation(program.gl, "u_ColorCorrection"))

            val uObjColor = Uniform(GLES30.glGetUniformLocation(program.gl, "u_ObjColor"))


            checkGLError("fragment shader parameters")

            return CubeShader(
                program,
                uModelView,
                uModelViewProjection,
                vPosition,
                vNormal,
                uLighting,
                uMaterial,
                uColorCorrection,
                uObjColor
            )
        }
    }
}