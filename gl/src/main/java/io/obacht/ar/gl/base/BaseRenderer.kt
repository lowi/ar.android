package io.obacht.ar.gl.base

import io.obacht.ar.domain.entity.gl.AssetMap
import io.obacht.ar.gl.scene.SceneParameters

interface BaseRenderer {
    fun initialize(assets: AssetMap)
}


/**
 * ArImageScene is  the base class for a scene rendered when viewing an augmented image.
 */
interface ArImageScene : BaseRenderer {

    fun draw(sceneParameters: SceneParameters)

}