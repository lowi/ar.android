package io.obacht.ar.gl.utils

// One way to set up an AugmentedImageDatabase instance,
// is to load a pre-existing augmented image database.
// This is its filename:
const val AUGMENTED_IMAGE_DB_NAME = "images.imgdb"
