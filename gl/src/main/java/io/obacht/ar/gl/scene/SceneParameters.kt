package io.obacht.ar.gl.scene

import com.google.ar.core.Anchor
import com.google.ar.core.AugmentedImage
import io.obacht.ar.domain.entity.gl.Light


/**
 * Wrapper for the data passed from ar core to the scene on each update
 */
data class SceneParameters(
    val viewMatrix: FloatArray,
    val projectionMatrix: FloatArray,
    val augmentedImage: AugmentedImage,
    val centerAnchor: Anchor,
    val light: Light
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SceneParameters

        if (!viewMatrix.contentEquals(other.viewMatrix)) return false
        if (!projectionMatrix.contentEquals(other.projectionMatrix)) return false
        if (augmentedImage != other.augmentedImage) return false
        if (centerAnchor != other.centerAnchor) return false
        if (light != other.light) return false

        return true
    }

    override fun hashCode(): Int {
        var result = viewMatrix.contentHashCode()
        result = 31 * result + projectionMatrix.contentHashCode()
        result = 31 * result + augmentedImage.hashCode()
        result = 31 * result + centerAnchor.hashCode()
        result = 31 * result + light.hashCode()
        return result
    }
}