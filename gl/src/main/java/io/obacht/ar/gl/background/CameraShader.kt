package io.obacht.ar.gl.background

import android.opengl.GLES30
import io.obacht.ar.domain.entity.gl.AssetMap
import io.obacht.ar.domain.entity.gl.ShaderSource
import io.obacht.ar.gl.base.GlHandle
import io.obacht.ar.gl.base.Shader
import io.obacht.ar.gl.base.compile
import java.nio.FloatBuffer

class CameraShader(
    program: GlHandle.Program,
    private val position: GlHandle.Varying,
    private val textureCoordinates: GlHandle.Varying,
    private val textureUniform: GlHandle.Uniform
) : Shader(program) {


    fun activate(quadCoordinates: FloatBuffer, quadTexCoordinates: FloatBuffer) {

        GLES30.glUseProgram(program.gl)
        GLES30.glUniform1i(textureUniform.gl, 0)

        // Set the vertex positions and texture coordinates.
        GLES30.glVertexAttribPointer(
            position.gl,
            CameraRenderer.COORDS_PER_VERTEX,
            GLES30.GL_FLOAT,
            false,
            0,
            quadCoordinates
        )
        GLES30.glVertexAttribPointer(
            textureCoordinates.gl,
            CameraRenderer.TEXTURE_COORDINATES_PER_VERTEX,
            GLES30.GL_FLOAT,
            false,
            0,
            quadTexCoordinates
        )
        GLES30.glEnableVertexAttribArray(position.gl)
        GLES30.glEnableVertexAttribArray(textureCoordinates.gl)
    }

    fun deactivate() {
        GLES30.glDisableVertexAttribArray(position.gl)
        GLES30.glDisableVertexAttribArray(textureCoordinates.gl)
    }

    companion object {

        fun initialize(assets: AssetMap): CameraShader {
            val vertexSource = assets["shader_camera_vertex"] as ShaderSource<*>
            val fragmentSource = assets["shader_camera_fragment"] as ShaderSource<*>

            val vertexHandle = vertexSource.compile()
            val fragmentHandle = fragmentSource.compile()

            // attach and link the shaders
            val program = GlHandle.Program(GLES30.glCreateProgram())
            GLES30.glAttachShader(program.gl, vertexHandle.gl)
            GLES30.glAttachShader(program.gl, fragmentHandle.gl)
            GLES30.glLinkProgram(program.gl)
            GLES30.glUseProgram(program.gl)

            // handle the shader uniforms and attributes
            val position =
                GlHandle.Varying(GLES30.glGetAttribLocation(program.gl, "a_Position"))
            val textureCoordinates =
                GlHandle.Varying(GLES30.glGetAttribLocation(program.gl, "a_TexCoord"))
            checkGLError("Program creation")
            val textureUniform =
                GlHandle.Uniform(GLES30.glGetUniformLocation(program.gl, "sTexture"))
            checkGLError("Program parameters")

            return CameraShader(
                program,
                position,
                textureCoordinates,
                textureUniform
            )
        }
    }

}
