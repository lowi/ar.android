package io.obacht.ar.gl.base

import android.opengl.GLES30
import arrow.core.Either
import io.obacht.ar.domain.ArError
import io.obacht.ar.domain.entity.gl.GlType
import io.obacht.ar.domain.entity.gl.ShaderSource


// A ShaderHandle is the index of a shader inside the glContext.
// Assigned via the GLES30.glCreateShader return value

sealed class GlHandle(val gl: Int) {
    class Shader(value: Int) : GlHandle(value)
    class Program(value: Int) : GlHandle(value)
    class Varying(value: Int) : GlHandle(value)
    class Uniform(value: Int) : GlHandle(value)
    class VertexBuffer(value: Int) : GlHandle(value)
    class Texture(value: Int) : GlHandle(value)
}


fun <S : GlType.Shader> ShaderSource<S>.compile(): GlHandle.Shader {
    val shader = GLES30.glCreateShader(shaderType.gl)

    // add the source code to the shader and compile it
    GLES30.glShaderSource(shader, content)
    GLES30.glCompileShader(shader)

    // This checks for for compilation errors
    val compiled = IntArray(1)
    GLES30.glGetShaderiv(shader, GLES30.GL_COMPILE_STATUS, compiled, 0)

    return if (compiled[0] == 0)
        throw ArError.Shader.Compile(GLES30.glGetShaderInfoLog(shader))
    else GlHandle.Shader(shader)
}


// Abstract wrapper encapsulating a compiled shader program.
// The one thing all shaders have in common is their gl_index/gl_id/'pointer'/whatever
// Subclasses extend this base class with uniforms/variables and such.
abstract class Shader(
    val program: GlHandle.Program
) {

    companion object {

        fun checkGLError(label: String) {
            val lastError = drainErrorQueue()
            if (lastError != GLES30.GL_NO_ERROR) {
                throw RuntimeException("$label: glError $lastError")
            }
        }

        // Drain the queue of all errors.
        private fun drainErrorQueue(): Int {
            var lastError = GLES30.GL_NO_ERROR
            var error: Int
            while (GLES30.glGetError().also { error = it } != GLES30.GL_NO_ERROR) {
                lastError = error
            }
            return lastError
        }

    }
}