package io.obacht.ar.gl.scene

import com.google.ar.core.Pose
import io.obacht.ar.domain.entity.gl.AssetMap
import io.obacht.ar.gl.base.ArImageScene
import io.obacht.ar.gl.images.BoxRenderer

class SimpleBoxScene : ArImageScene {

    private val box = BoxRenderer()

    // reusable model matrix buffer
    private val modelMatrix = FloatArray(16)

    override fun initialize(assets: AssetMap) {
        box.initialize(assets)
    }

    override fun draw(sceneParameters: SceneParameters) {

        // 'destructuring' because I'm to lazy to refactor properly
        // todo: clean up
        val augmentedImage = sceneParameters.augmentedImage
        val centerAnchor = sceneParameters.centerAnchor
        val viewMatrix = sceneParameters.viewMatrix
        val projectionMatrix = sceneParameters.projectionMatrix
        val light = sceneParameters.light

        convertHexToColor(TINT_COLORS_HEX[augmentedImage.index % TINT_COLORS_HEX.size])

        // those are the corners of the detected image
        val localBoundaryPoses = arrayOf(
            Pose.makeTranslation(
                -0.5f * augmentedImage.extentX,
                0.0f,
                -0.5f * augmentedImage.extentZ
            ),  // upper left
            Pose.makeTranslation(
                0.5f * augmentedImage.extentX,
                0.0f,
                -0.5f * augmentedImage.extentZ
            ),  // upper right
            Pose.makeTranslation(
                0.5f * augmentedImage.extentX,
                0.0f,
                0.5f * augmentedImage.extentZ
            ),  // lower right
            Pose.makeTranslation(
                -0.5f * augmentedImage.extentX,
                0.0f,
                0.5f * augmentedImage.extentZ
            ) // lower left
        )

        // this is the anchor position in the world-model-space
        val anchorPose = centerAnchor.pose

        // translate the the local boundary poses into world-coordinates
        val worldBoundaryPoses = arrayOfNulls<Pose>(4)
        for (i in 0..3) {
            worldBoundaryPoses[i] = anchorPose.compose(localBoundaryPoses[i])
        }

        // render the cube right at the center
        anchorPose.toMatrix(modelMatrix, 0)

//        cube.updateModelMatrix(modelMatrix, 0.1f)
//        cube.draw(viewMatrix, projectionMatrix, light)

        box.updateModelMatrix(modelMatrix, 0.1f)
        box.draw(viewMatrix, projectionMatrix, light, floatArrayOf(0.9f, 0.9f, 0.5f, 1.0f))

//        box.updateModelMatrix(modelMatrix, 0.1f)
//        box.draw(viewMatrix, projectionMatrix, light, floatArrayOf(0.9f, 0.9f, 0.5f, 1.0f))
    }

    companion object {
        private const val TINT_INTENSITY = 0.1f
        private const val TINT_ALPHA = 1.0f
        private val TINT_COLORS_HEX = intArrayOf(
            0x000000,
            0xF44336,
            0xE91E63,
            0x9C27B0,
            0x673AB7,
            0x3F51B5,
            0x2196F3,
            0x03A9F4,
            0x00BCD4,
            0x009688,
            0x4CAF50,
            0x8BC34A,
            0xCDDC39,
            0xFFEB3B,
            0xFFC107,
            0xFF9800
        )

        private fun convertHexToColor(colorHex: Int): FloatArray {
            // colorHex is in 0xRRGGBB format
            val red = (colorHex and 0xFF0000 shr 16) / 255.0f * TINT_INTENSITY
            val green = (colorHex and 0x00FF00 shr 8) / 255.0f * TINT_INTENSITY
            val blue = (colorHex and 0x0000FF) / 255.0f * TINT_INTENSITY
            return floatArrayOf(red, green, blue, TINT_ALPHA)
        }
    }

}