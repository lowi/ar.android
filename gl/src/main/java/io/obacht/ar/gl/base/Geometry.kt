package io.obacht.ar.gl.base

import android.opengl.GLES30
import arrow.core.Tuple4
import arrow.fx.IO
import arrow.fx.extensions.fx
import de.javagl.obj.Obj
import de.javagl.obj.ObjData
import de.javagl.obj.ObjReader
import de.javagl.obj.ObjUtils
import io.obacht.ar.domain.entity.gl.GlAsset
import io.obacht.ar.domain.entity.gl.GlType
import io.obacht.ar.domain.entity.gl.ObjGeometry
import io.obacht.ar.domain.util.BYTES_PER_FLOAT
import io.obacht.ar.domain.util.BYTES_PER_SHORT
import java.io.InputStream
import java.nio.*


/**
 * Wrapper for the geometry data
 */
data class Geometry(
    val vertexBuffer: GlHandle.VertexBuffer,
    val index: GlHandle.VertexBuffer,
    val indexCount: Int,
    val verticesBaseAddress: Int,
    val normalsBaseAddress: Int
)


fun ObjGeometry.compile(): Geometry {

    // Prepare buffers
    val numBuffers = 2
    val buffers = IntArray(numBuffers)
    GLES30.glGenBuffers(numBuffers, buffers, 0)
    val vertexBufferHandle = GlHandle.VertexBuffer(buffers[0])
    val indexBufferHandle = GlHandle.VertexBuffer(buffers[1])


    // Load vertex buffer
    // ——————————————————

    // Position buffer
    val positionCapacity = vertices.limit() * BYTES_PER_FLOAT
    val positionByteBuffer = ByteBuffer.allocateDirect(positionCapacity)
    positionByteBuffer.order(ByteOrder.nativeOrder())

    val positionBuffer = positionByteBuffer.asFloatBuffer()
    positionBuffer.put(vertices)
    positionBuffer.position(0)

    // Normal buffer
    val normalCapacity = normals.limit() * BYTES_PER_FLOAT
    val normalsByteBuffer = ByteBuffer.allocateDirect(normalCapacity)
    normalsByteBuffer.order(ByteOrder.nativeOrder())

    val normalsBuffer = normalsByteBuffer.asFloatBuffer()
    normalsBuffer.put(normals)
    normalsBuffer.position(0)


    val verticesBaseAddress = 0
    val normalsBaseAddress = verticesBaseAddress + positionCapacity
    val totalBytes: Int = normalsBaseAddress + normalCapacity


    GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vertexBufferHandle.gl)
    GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER, totalBytes, null, GLES30.GL_STATIC_DRAW)

    GLES30.glBufferSubData(
        GLES30.GL_ARRAY_BUFFER, verticesBaseAddress, positionCapacity, positionBuffer
    )
    GLES30.glBufferSubData(
        GLES30.GL_ARRAY_BUFFER, normalsBaseAddress, normalCapacity, normalsBuffer
    )
    GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, 0)


    // Index buffer
    // ————————————
    val indexCapacity = vertices.limit() * BYTES_PER_SHORT
    val indexByteBuffer = ByteBuffer.allocateDirect(indexCapacity)
    indexByteBuffer.order(ByteOrder.nativeOrder())

    val indexBuffer = indexByteBuffer.asShortBuffer()
    indexBuffer.put(indices)
    indexBuffer.rewind()

    GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, indexBufferHandle.gl)
    GLES30.glBufferData(
        GLES30.GL_ARRAY_BUFFER, indexCapacity, indexBuffer,
        GLES30.GL_STATIC_DRAW
    )

    // release
    Shader.checkGLError("geometry buffers load")
    GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, 0)

    return Geometry(
        vertexBufferHandle,
        indexBufferHandle,
        indexCount = vertices.limit(),
        verticesBaseAddress,
        normalsBaseAddress
    )
}