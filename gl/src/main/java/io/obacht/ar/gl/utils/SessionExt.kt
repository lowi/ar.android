package io.obacht.ar.gl.utils

import arrow.fx.IO
import com.google.ar.core.Config
import com.google.ar.core.Session as ArSession
import com.google.ar.core.exceptions.*
import io.obacht.ar.domain.ArError
import io.obacht.ar.domain.repository.ArRepository


fun ArSession.configure(arRepository: ArRepository): IO<ArSession> =
    config(this, arRepository)


private fun config(session: ArSession, arRepository: ArRepository): IO<ArSession> =
    try {
        val config = session.config

        // configure depth API.
        // (Always disabled since my phone doesn't support it :( )
        if (session.isDepthModeSupported(Config.DepthMode.AUTOMATIC)) {
            config.depthMode = Config.DepthMode.AUTOMATIC
        } else {
            config.depthMode = Config.DepthMode.DISABLED
        }

        // enable auto-focus
        config.focusMode = Config.FocusMode.AUTO

        // set light estimation strategy
        //            config.lightEstimationMode = Config.LightEstimationMode.ENVIRONMENTAL_HDR
        config.lightEstimationMode = Config.LightEstimationMode.AMBIENT_INTENSITY


        // load the image database for augmented images
        // @see: https://developers.google.com/ar/develop/c/augmented-images
        arRepository.loadImageDB(AUGMENTED_IMAGE_DB_NAME, session)
            .map {
                config.augmentedImageDatabase = it

                // actual configuration!
                session.configure(config)

                // return the session
                session
            }

    } catch (e: UnavailableException) {
        IO.raiseError(ArError.ArCore.NotInstalled)
    } catch (e: UnavailableUserDeclinedInstallationException) {
        IO.raiseError(ArError.ArCore.NotInstalled)
    } catch (e: UnavailableApkTooOldException) {
        IO.raiseError(ArError.ArCore.OutOfDate)
    } catch (e: UnavailableSdkTooOldException) {
        IO.raiseError(ArError.ArCore.OutOfDate)
    } catch (e: UnavailableDeviceNotCompatibleException) {
        IO.raiseError(ArError.ArCore.NotSupported)
    } catch (e: Exception) {
        IO.raiseError(ArError.ArCore.Unknown)
    }
