package io.obacht.ar.gl.images

import android.opengl.GLES30.*
import android.opengl.Matrix
import io.obacht.ar.domain.entity.gl.AssetMap
import io.obacht.ar.domain.entity.gl.Light
import io.obacht.ar.domain.entity.gl.ObjGeometry
import io.obacht.ar.gl.base.BaseRenderer
import io.obacht.ar.gl.base.Geometry
import io.obacht.ar.gl.base.Shader
import io.obacht.ar.gl.base.compile
import io.obacht.ar.gl.utils.GLHelper


class BoxRenderer : BaseRenderer {

    private lateinit var boxGeometry: Geometry
    private lateinit var stencilGeometry: Geometry

    private lateinit var geometryShader: CubeShader
    private lateinit var stencilShader: StencilShader

    // Temporary matrices allocated here to reduce number of allocations for each frame.
    private val modelMatrix = FloatArray(16)
    private val mModelView = FloatArray(16)
    private val mModelViewProjection = FloatArray(16)
    private val viewLightDirection = FloatArray(4)


    override fun initialize(assets: AssetMap) {
        geometryShader = CubeShader.initialize(assets)
        stencilShader = StencilShader.initialize(assets)

        val boxAsset = assets["geometry_box"] as ObjGeometry
        boxGeometry = boxAsset.compile()

        val stencilAsset = assets["geometry_stencil"] as ObjGeometry
        stencilGeometry = stencilAsset.compile()

    }


    /**
     * Updates the object model matrix and applies scaling.
     *
     * @param modelMatrix A 4x4 model-to-world transformation matrix, stored in column-major order.
     * @param scaleFactor A separate scaling factor to apply before the `modelMatrix`.
     */
    fun updateModelMatrix(modelMatrix: FloatArray, scaleFactor: Float) {
        val scaleMatrix = GLHelper.identityMatrix(16)
        scaleMatrix[0] = scaleFactor
        scaleMatrix[5] = scaleFactor
        scaleMatrix[10] = scaleFactor
        Matrix.multiplyMM(this.modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0)
    }

    /**
     * Draws the model.
     *
     * @param cameraView A 4x4 view matrix, in column-major order.
     * @param cameraPerspective A 4x4 projection matrix, in column-major order.
     * @see .updateModelMatrix
     */
    fun draw(
        cameraView: FloatArray,
        cameraPerspective: FloatArray,
        light: Light,
        objColor: FloatArray
    ) {
        Shader.checkGLError("Before draw")

        // Build the ModelView and ModelViewProjection matrices
        // for calculating object position and light.
        Matrix.multiplyMM(mModelView, 0, cameraView, 0, modelMatrix, 0)
        Matrix.multiplyMM(mModelViewProjection, 0, cameraPerspective, 0, mModelView, 0)

        drawStencil()
        drawBox(geometryShader, light, objColor)


    }

    private fun drawBox(shader: CubeShader, light: Light, objColor: FloatArray) {

        // turn on the geometry shader
        glUseProgram(shader.program.gl)

        // Set the lighting environment properties.
        Matrix.multiplyMV(
            viewLightDirection,
            0,
            mModelView,
            0,
            LIGHT_DIRECTION,
            0
        )
        GLHelper.normalizeVec3(viewLightDirection)
        glUniform4f(
            shader.uLighting.gl,
            viewLightDirection[0],
            viewLightDirection[1],
            viewLightDirection[2],
            1f
        )
        glUniform4fv(shader.uColorCorrection.gl, 1, light.colorCorrection, 0)

        // Set the object color property.
        glUniform4fv(shader.uObjColor.gl, 1, objColor, 0)

        // Set the object material properties.
        // Set some default material properties to use for lighting.
        val ambient = 0.3f
        val diffuse = 1.0f
        val specular = 1.0f
        val specularPower = 6.0f
        glUniform4f(shader.uMaterial.gl, ambient, diffuse, specular, specularPower)
        Shader.checkGLError("light")


        // Set the vertex attributes:
        // ——————————————————————————
        glBindBuffer(GL_ARRAY_BUFFER, boxGeometry.vertexBuffer.gl)
        Shader.checkGLError("vertex bind")

        glVertexAttribPointer(
            shader.vPosition.gl,
            COORDINATES_PER_VERTEX,
            GL_FLOAT,
            false,
            0,
            boxGeometry.verticesBaseAddress
        )
        Shader.checkGLError("vPosition")
        glVertexAttribPointer(
            shader.vNormal.gl,
            COORDINATES_PER_VERTEX,
            GL_FLOAT,
            false,
            0,
            boxGeometry.normalsBaseAddress
        )
        Shader.checkGLError("vNormal")
        glBindBuffer(GL_ARRAY_BUFFER, 0)


        // Set the ModelViewProjection matrix in the shader.
        glUniformMatrix4fv(shader.uModelView.gl, 1, false, mModelView, 0)
        glUniformMatrix4fv(
            shader.uModelView.gl,
            1,
            false,
            mModelView,
            0
        )

        // Apply the projection and view transformation
        glUniformMatrix4fv(
            shader.uModelViewProjection.gl,
            1,
            false,
            mModelViewProjection,
            0
        )
        Shader.checkGLError("After ModelViewProjection transform")

        // Enable vertex arrays


        // obacht: we're NOT doing backface-culling (the usual usage for culling)
        //  instead we're culling the front so that we can see 'inside' the thing.
//        glCullFace(GL_FRONT)
//        glEnable(GL_CULL_FACE)

        glStencilFunc(GL_EQUAL, 1, 0xff)
        glStencilMask(0x00)
        glDepthMask(true)
        glColorMask(true, true, true, true)

        // draw
        glEnableVertexAttribArray(shader.vPosition.gl)
        glEnableVertexAttribArray(shader.vNormal.gl)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, boxGeometry.index.gl)
        glDrawElements(GL_TRIANGLES, boxGeometry.indexCount, GL_UNSIGNED_SHORT, 0)


        // release
//        glStencilMask(0xFF)
//        glStencilFunc(GL_ALWAYS, 1, 0xFF)
//        glEnable(GL_DEPTH_TEST)

//        glDisable(GL_CULL_FACE)

        glDisableVertexAttribArray(shader.vPosition.gl)
        glDisableVertexAttribArray(shader.vNormal.gl)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)

        Shader.checkGLError("After release")

    }

    private fun drawStencil() {

        // turn on the shader
        glUseProgram(stencilShader.program.gl)

        // Set the vertex attributes:
        // ——————————————————————————
        glBindBuffer(GL_ARRAY_BUFFER, stencilGeometry.vertexBuffer.gl)
        Shader.checkGLError("vertex bind")

        glVertexAttribPointer(
            stencilShader.vPosition.gl,
            COORDINATES_PER_VERTEX,
            GL_FLOAT,
            false,
            0,
            stencilGeometry.verticesBaseAddress
        )
        Shader.checkGLError("vPosition")
        glBindBuffer(GL_ARRAY_BUFFER, 0)


        glUniformMatrix4fv(stencilShader.uModelView.gl, 1, false, mModelView, 0)
        glUniformMatrix4fv(
            stencilShader.uModelView.gl,
            1,
            false,
            mModelView,
            0
        )

        glUniformMatrix4fv(
            stencilShader.uModelViewProjection.gl,
            1,
            false,
            mModelViewProjection,
            0
        )
        Shader.checkGLError("After ModelViewProjection transform")


        // Prepare stencil buffer
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE)

        glStencilFunc(GL_ALWAYS, 1, 0xff)
        glStencilMask(0xff)
        glDepthMask(false)
        glColorMask(false, false, false, false)


        // draw
        glEnableVertexAttribArray(stencilShader.vPosition.gl)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, stencilGeometry.index.gl)
        glDrawElements(GL_TRIANGLES, stencilGeometry.indexCount, GL_UNSIGNED_SHORT, 0)


        // release
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
        glDisableVertexAttribArray(stencilShader.vPosition.gl)

        Shader.checkGLError("After release")
    }


    companion object {

        const val COORDINATES_PER_VERTEX = 3

        // Note: the last component must be zero to avoid applying the translational part of the matrix.
        private val LIGHT_DIRECTION = floatArrayOf(0.250f, 0.866f, 0.433f, 0.0f)

    }

}
