package io.obacht.ar.gl.di

import io.obacht.ar.gl.GlRenderer
import io.obacht.ar.gl.background.CameraRenderer
import io.obacht.ar.gl.images.BoxRenderer
import org.koin.dsl.module


val glModule = module {

    factory {
        CameraRenderer()
    }

    factory {
        BoxRenderer()
    }

    factory {
        GlRenderer(cameraRenderer = get())
    }

}
