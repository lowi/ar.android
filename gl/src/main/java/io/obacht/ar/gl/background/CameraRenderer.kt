package io.obacht.ar.gl.background

import android.opengl.GLES11Ext
import android.opengl.GLES30
import com.google.ar.core.Coordinates2d
import com.google.ar.core.Frame
import io.obacht.ar.domain.entity.gl.AssetMap
import io.obacht.ar.gl.base.BaseRenderer
import io.obacht.ar.gl.base.GlHandle
import io.obacht.ar.gl.base.Shader
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer


/**
 * Renderer for the camera view used as the backdrop for our AR shenanigans.
 */
class CameraRenderer : BaseRenderer {

    private val quadCoordinates: FloatBuffer
    private val quadTextureCoordinates: FloatBuffer

    private lateinit var cameraShader: CameraShader

    lateinit var textureId: GlHandle.Texture
        private set

    /**
     * Initialize the buffers for the surface (vertex coordinates & texture coordinates)
     */
    init {
        // weird. let's leave it here for now
        val numVertices = 4
        if (numVertices != QUAD_COORDS.size / COORDS_PER_VERTEX) {
            throw RuntimeException("Unexpected number of vertices in BackgroundRenderer.")
        }

        // coordinates
        val bbCoordinates = ByteBuffer.allocateDirect(QUAD_COORDS.size * FLOAT_SIZE)
        bbCoordinates.order(ByteOrder.nativeOrder())

        quadCoordinates = bbCoordinates.asFloatBuffer()
        quadCoordinates.put(QUAD_COORDS)
        quadCoordinates.position(0)

        // texture coordinates
        val bbTexCoordinatesTransformed =
            ByteBuffer.allocateDirect(numVertices * TEXTURE_COORDINATES_PER_VERTEX * FLOAT_SIZE)
        bbTexCoordinatesTransformed.order(ByteOrder.nativeOrder())

        quadTextureCoordinates = bbTexCoordinatesTransformed.asFloatBuffer()
    }

    private var suppressTimestampZeroRendering = true


    override fun initialize(assets: AssetMap) {
        cameraShader = CameraShader.initialize(assets)
        textureId = generateAndBindTexture()
    }


    /**
     * Generate a texture for rendering the camera image and bind it to an
     * GL_TEXTURE_EXTERNAL_OES texture-target
     * https://www.khronos.org/registry/OpenGL/extensions/OES/OES_EGL_image_external.txt
     *
     * @return the id of the generated texture in the glContext
     */
    private fun generateAndBindTexture(): GlHandle.Texture {

        // Generate the background texture.
        val textures = IntArray(1)
        GLES30.glGenTextures(1, textures, 0)

        // get the index assigned by the glContext
        val textureId = textures[0]

        // bind the texture
        // obacht: I have no clue why 'GLES11Ext' is used here.
        //  the code is taken from https://github.com/google-ar/arcore-android-sdk/blob/master/samples/hello_ar_java/app/src/main/java/com/google/ar/core/examples/java/common/rendering/BackgroundRenderer.java
        val textureTarget = GLES11Ext.GL_TEXTURE_EXTERNAL_OES
        GLES30.glBindTexture(textureTarget, textureId)
        GLES30.glTexParameteri(textureTarget, GLES30.GL_TEXTURE_WRAP_S, GLES30.GL_CLAMP_TO_EDGE)
        GLES30.glTexParameteri(textureTarget, GLES30.GL_TEXTURE_WRAP_T, GLES30.GL_CLAMP_TO_EDGE)
        GLES30.glTexParameteri(textureTarget, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_LINEAR)
        GLES30.glTexParameteri(textureTarget, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_LINEAR)

        return GlHandle.Texture(textureId)
    }


//    fun suppressTimestampZeroRendering(suppressTimestampZeroRendering: Boolean) {
//        this.suppressTimestampZeroRendering = suppressTimestampZeroRendering
//    }

    /**
     * Draws the AR background image. The image will be drawn such that virtual content rendered with
     * the matrices provided by [com.google.ar.core.Camera.getViewMatrix] and
     * [com.google.ar.core.Camera.getProjectionMatrix] will
     * accurately follow static physical objects. This must be called **before** drawing virtual
     * content.
     *
     * @param frame The current `Frame` as returned by Session.update2.
     */
    fun draw(frame: Frame) {
        // If display rotation changed (also includes view size change), we need to re-query the uv
        // coordinates for the screen rect, as they may have changed as well.
        if (frame.hasDisplayGeometryChanged()) {
            frame.transformCoordinates2d(
                Coordinates2d.OPENGL_NORMALIZED_DEVICE_COORDINATES,
                quadCoordinates,
                Coordinates2d.TEXTURE_NORMALIZED,
                quadTextureCoordinates
            )
        }
        if (frame.timestamp == 0L && suppressTimestampZeroRendering) {
            // Suppress rendering if the camera did not produce the first frame yet. This is to avoid
            // drawing possible leftover data from previous sessions if the texture is reused.
            return
        }
        draw()
    }

    /**
     * Draws the camera background image using the currently configured [ ][CameraRenderer.quadTextureCoordinates] image texture coordinates.
     */
    private fun draw() {
        // Ensure position is rewound before use.
        quadTextureCoordinates.position(0)

        // No need to test or write depth, the screen quad has arbitrary depth, and is expected
        // to be drawn first.
        GLES30.glDisable(GLES30.GL_DEPTH_TEST)
        GLES30.glDepthMask(false)
        GLES30.glActiveTexture(GLES30.GL_TEXTURE0)

        GLES30.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textureId.gl)
        cameraShader.activate(quadCoordinates, quadTextureCoordinates)

        GLES30.glDrawArrays(GLES30.GL_TRIANGLE_STRIP, 0, 4)

        cameraShader.deactivate()

        // Restore the depth state for further drawing.
        GLES30.glDepthMask(true)
        GLES30.glEnable(GLES30.GL_DEPTH_TEST)
        Shader.checkGLError("BackgroundRendererDraw")
    }

    companion object {

        internal const val COORDS_PER_VERTEX = 2
        internal const val TEXTURE_COORDINATES_PER_VERTEX = 2
        private const val FLOAT_SIZE = 4

        /**
         * (-1, 1) ------- (1, 1)
         * |    \           |
         * |       \        |
         * |          \     |
         * |             \  |
         * (-1, -1) ------ (1, -1)
         * Ensure triangles are front-facing, to support glCullFace().
         * This quad will be drawn using GL_TRIANGLE_STRIP which draws two
         * triangles: v0->v1->v2, then v2->v1->v3.
         */
        private val QUAD_COORDS = floatArrayOf(
            -1.0f, -1.0f, +1.0f, -1.0f, -1.0f, +1.0f, +1.0f, +1.0f
        )
    }

}
