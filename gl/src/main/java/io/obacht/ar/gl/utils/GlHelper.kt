package io.obacht.ar.gl.utils

import android.opengl.Matrix
import kotlin.math.sqrt


object GLHelper {

    fun identityMatrix(size: Int): FloatArray {
        val matrix = FloatArray(size)
        Matrix.setIdentityM(matrix, 0)
        return matrix
    }


    fun normalizeVec3(v: FloatArray) {
        val reciprocalLength =
            1.0f / sqrt(v[0] * v[0] + v[1] * v[1] + (v[2] * v[2]).toDouble()).toFloat()
        v[0] *= reciprocalLength
        v[1] *= reciprocalLength
        v[2] *= reciprocalLength
    }

}
