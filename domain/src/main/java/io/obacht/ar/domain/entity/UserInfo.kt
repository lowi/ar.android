package io.obacht.ar.domain.entity


data class UserInfo(
    val login: String,
    val id: Int,
    val name: String,
)
