package io.obacht.ar.domain.db

//@Database(
//    entities = [ReceivedEvent::class, Repo::class],
//    version = 1
//)
//abstract class UserDatabase : RoomDatabase() {
abstract class UserDatabase {

    abstract fun userReceivedEventDao(): UserReceivedEventDao

    abstract fun userReposDao(): UserReposDao
}