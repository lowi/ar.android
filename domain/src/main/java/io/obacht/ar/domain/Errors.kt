package io.obacht.ar.domain

import io.obacht.ar.domain.entity.gl.GlType


sealed class ArError : Error() {

    object EmptyInputError : ArError()

    class Permissions(val msg: String) : ArError()
    class PermissionsRationale(val msg: String) : ArError()

    sealed class Camera : ArError() {
        object Permission : Camera()
        object PermissionRationale : Camera()
    }

    sealed class ArCore : ArError() {
        class ImageDB(val msg: String) : ArCore()
        object NotSupported : ArCore()
        object NotInstalled : ArCore()
        object OutOfDate : ArCore()
        object Unknown : ArCore()

        sealed class Apk : ArCore() {
            object InstallationRequested : ArCore()
            object Unknown : Apk()
            object UnknownChecking : Apk()
            object UnknownTimeout : Apk()
            object DeviceIncapable : Apk()
            object NotInstalled : Apk()
            object TooOld : Apk()
        }
    }

    sealed class Gl : ArError() {
        sealed class Asset : Gl() {
            class Type(val t: GlType) : Asset()
        }
    }

    sealed class Shader : ArError() {
        object Load : Shader()
        class Compile(val msg: String) : Shader()
    }

    sealed class Storage : ArError() {
        class NotFound(val msg: String) : Storage()
        object Unknown : Storage()
    }


}