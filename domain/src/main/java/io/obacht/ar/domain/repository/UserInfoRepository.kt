package io.obacht.ar.domain.repository

import android.content.SharedPreferences
import io.obacht.ar.architecture.core.util.SingletonHolderSingleArg
import io.obacht.ar.architecture.core.util.prefs.boolean
import io.obacht.ar.architecture.core.util.prefs.string

class UserInfoRepository(prefs: SharedPreferences) {

    var accessToken: String by prefs.string("user_access_token", "")

    var username by prefs.string("username", "")

    var password by prefs.string("password", "")

    var isAutoLogin: Boolean by prefs.boolean("auto_login", true)

    companion object :
            SingletonHolderSingleArg<UserInfoRepository, SharedPreferences>(::UserInfoRepository)
}