package io.obacht.ar.domain.db

import androidx.paging.DataSource
import io.obacht.ar.domain.entity.Repo

interface UserReposDao {

    suspend fun insert(repos: List<Repo>)

    fun queryRepos(): DataSource.Factory<Int, Repo>

    suspend fun deleteAllRepos()

    suspend fun getNextIndexInRepos(): Int?
}