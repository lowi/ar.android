package io.obacht.ar.domain.repository

import arrow.fx.IO
import arrow.fx.extensions.fx
import com.google.ar.core.AugmentedImageDatabase
import com.google.ar.core.Session
import io.obacht.ar.domain.entity.gl.*

interface ArRepository {

//    fun loadShaders(vararg filenames: String): IO<List<ShaderSource>>
//
//    fun loadShader(filename: String): IO<ShaderSource>
//
//    fun loadGeometry(filename: String): IO<Geometry>
//
//    fun loadTexture(filename: String): IO<Texture>
//
//    fun loadImage(filename: String): IO<Image>
//

    fun <T : GlType> loadAsset(glFile: GlFile<T>): IO<GlAsset<T>>

    fun loadImageDB(filename: String, session: Session): IO<AugmentedImageDatabase>

    fun loadAssets(assets: Map<AssetId, GlFile<GlType>>): IO<Map<AssetId, GlAsset<GlType>>> =
        IO.fx {
            assets
                .map { Pair(it.key, loadAsset(it.value).bind()) }
                .associate { it }
        }


}