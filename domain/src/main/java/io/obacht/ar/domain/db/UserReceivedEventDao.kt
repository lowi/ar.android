package io.obacht.ar.domain.db

import androidx.annotation.WorkerThread
import androidx.paging.DataSource
import io.obacht.ar.domain.entity.ReceivedEvent

interface UserReceivedEventDao {

    @WorkerThread
    suspend fun insert(receivedEvents: List<ReceivedEvent>)

    @WorkerThread
    fun queryEvents(): DataSource.Factory<Int, ReceivedEvent>

    @WorkerThread
    suspend fun clearReceivedEvents()

    @WorkerThread
    suspend fun getNextIndexInReceivedEvents(): Int?
}