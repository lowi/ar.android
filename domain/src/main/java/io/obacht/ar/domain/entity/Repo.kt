package io.obacht.ar.domain.entity

data class Repo(
    val id: Long,
    val nodeId: String,
    val name: String,
) {
    val indexInSortResponse: Int = -1
}

data class RepoOwner(
    val login: String,
    val id: Int,
    val nodeId: String,

    )

data class License(
    val key: String,
    val name: String,
    val url: String,
)

class ReposPersistentConverter {

    // RepoOwner
//    fun storeRepoOwnerToString(data: RepoOwner): String = data.toJson()
//
//    fun storeStringToRepoOwner(value: String): RepoOwner = value.fromJson()
//
//    // License
//    fun storeLicenseToString(data: License?): String = data.toJson()
//
//    fun storeStringToLicense(value: String): License? = value.fromJson()
}