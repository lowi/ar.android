package io.obacht.ar.domain.entity.gl

import com.google.ar.core.ArImage


data class Light(
    val intensity: FloatArray,
    val direction: FloatArray,
    val harmonics: FloatArray,
    val lightmaps: Array<ArImage>,
    val colorCorrection: FloatArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Light

        if (!intensity.contentEquals(other.intensity)) return false
        if (!direction.contentEquals(other.direction)) return false
        if (!harmonics.contentEquals(other.harmonics)) return false
        if (!lightmaps.contentEquals(other.lightmaps)) return false
        if (!colorCorrection.contentEquals(other.colorCorrection)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = intensity.contentHashCode()
        result = 31 * result + direction.contentHashCode()
        result = 31 * result + harmonics.contentHashCode()
        result = 31 * result + lightmaps.contentHashCode()
        result = 31 * result + colorCorrection.contentHashCode()
        return result
    }
}