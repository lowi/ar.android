package io.obacht.ar.domain.entity.gl

import android.opengl.GLES30
import arrow.fx.IO
import io.obacht.ar.domain.ArError
import java.nio.FloatBuffer
import java.nio.ShortBuffer


typealias AssetId = String
typealias AssetMap = Map<AssetId, GlAsset<GlType>>

/**
 * Type hierarchy of ar assets
 */
sealed class GlType {

    sealed class Shader(val gl: Int) : GlType() {
        object Vertex : Shader(GLES30.GL_VERTEX_SHADER)
        object Geometry : Shader(-1)
        object Fragment : Shader(GLES30.GL_FRAGMENT_SHADER)
    }

    object Geometry : GlType()
    object Texture : GlType()
}

/**
 * The path to a file tagged with its type
 */
data class GlFile<T : GlType>(
    val name: String,
    val type: T
)

/**
 * The contents of a file tagged with its type
 */
open class GlAsset<T : GlType>

data class ShaderSource<S : GlType.Shader>(
    val content: String,
    val shaderType: S
) : GlAsset<GlType.Shader>()


data class ObjGeometry(
    val indices: ShortBuffer,
    val vertices: FloatBuffer,
    val textureCoordinates: FloatBuffer,
    val normals: FloatBuffer,
) : GlAsset<GlType.Geometry>()


