package io.obacht.ar.domain.util


const val BYTES_PER_FLOAT = 4
const val BYTES_PER_SHORT = 2
