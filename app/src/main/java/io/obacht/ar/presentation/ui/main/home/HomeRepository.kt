package io.obacht.ar.presentation.ui.main.home

import android.annotation.SuppressLint
import androidx.annotation.AnyThread
import androidx.annotation.MainThread
import androidx.paging.DataSource
import io.obacht.ar.architecture.core.base.repository.BaseRepositoryBoth
import io.obacht.ar.architecture.core.base.repository.ILocalDataSource
import io.obacht.ar.architecture.core.base.repository.IRemoteDataSource
import io.obacht.ar.domain.db.UserDatabase
import io.obacht.ar.domain.entity.ReceivedEvent
import io.obacht.ar.presentation.base.Results
import io.obacht.ar.presentation.manager.UserManager
import io.obacht.ar.presentation.utils.PAGING_REMOTE_PAGE_SIZE

@SuppressLint("CheckResult")
class HomeRepository(
    remoteDataSource: HomeRemoteDataSource,
    localDataSource: HomeLocalDataSource
) : BaseRepositoryBoth<HomeRemoteDataSource, HomeLocalDataSource>(
    remoteDataSource,
    localDataSource
) {

    @MainThread
    suspend fun fetchEventByPage(
        pageIndex: Int,
        remoteRequestPerPage: Int = PAGING_REMOTE_PAGE_SIZE
    ): Results<List<ReceivedEvent>> {
        val username: String = UserManager.INSTANCE.login
        return remoteDataSource.fetchEventsByPage(username, pageIndex, remoteRequestPerPage)
    }

    @AnyThread
    suspend fun clearAndInsertNewData(items: List<ReceivedEvent>) {
        localDataSource.clearAndInsertNewData(items)
    }

    @AnyThread
    suspend fun insertNewPageData(items: List<ReceivedEvent>) {
        localDataSource.insertNewPagedEventData(items)
    }

    @MainThread
    fun fetchEventDataSourceFactory(): DataSource.Factory<Int, ReceivedEvent> {
        return localDataSource.fetchPagedListFromLocal()
    }
}

class HomeRemoteDataSource(
    // private val serviceManager: ServiceManager
) : IRemoteDataSource {

    suspend fun fetchEventsByPage(
        username: String,
        pageIndex: Int,
        perPage: Int
    ): Results<List<ReceivedEvent>> {
        return fetchEventsByPageInternal(username, pageIndex, perPage)
    }

    private suspend fun fetchEventsByPageInternal(
        username: String,
        pageIndex: Int,
        perPage: Int
    ): Results<List<ReceivedEvent>> {
//        val eventsResponse = serviceManager.userService
//            .queryReceivedEvents(username, pageIndex, perPage)
//        return processApiResponse(eventsResponse)
        TODO()
    }
}

@SuppressLint("CheckResult")
class HomeLocalDataSource(private val db: UserDatabase) : ILocalDataSource {

    fun fetchPagedListFromLocal(): DataSource.Factory<Int, ReceivedEvent> {
        return db.userReceivedEventDao().queryEvents()
    }

    suspend fun clearOldData() {
//        db.withTransaction {
//            db.userReceivedEventDao().clearReceivedEvents()
//        }
        TODO()
    }

    suspend fun clearAndInsertNewData(data: List<ReceivedEvent>) {
//        db.withTransaction {
//            db.userReceivedEventDao().clearReceivedEvents()
//            insertDataInternal(data)
//        }
        TODO()
    }

    suspend fun insertNewPagedEventData(newPage: List<ReceivedEvent>) {
//        db.withTransaction { insertDataInternal(newPage) }
        TODO()
    }

    private suspend fun insertDataInternal(newPage: List<ReceivedEvent>) {
//        val start = db.userReceivedEventDao().getNextIndexInReceivedEvents() ?: 0
//        val items = newPage.mapIndexed { index, child ->
//            child.indexInResponse = start + index
//            child
//        }
//        db.userReceivedEventDao().insert(items)
        TODO()
    }
}