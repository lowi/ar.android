package io.obacht.ar.presentation.ui.main.repos

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.paging.PagedList
import io.obacht.ar.architecture.core.base.view.fragment.BaseFragment
import io.obacht.ar.architecture.core.ext.observe
import io.obacht.ar.architecture.core.ext.jumpBrowser
import io.obacht.ar.image.R
import io.obacht.ar.domain.entity.Repo
import io.obacht.ar.presentation.utils.removeAllAnimation
import io.obacht.ar.presentation.utils.toast
import kotlinx.android.synthetic.main.fragment_repos.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ReposFragment : BaseFragment() {

    private val mViewModel: ReposViewModel by viewModel()

    override val layoutId: Int = R.layout.fragment_repos

    private val mAdapter = ReposPagedAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.inflateMenu(R.menu.menu_repos_filter_type)

        mRecyclerView.adapter = mAdapter
        mRecyclerView.removeAllAnimation()

        binds()
    }

    private fun binds() {
        // swipe refresh event.
//        mSwipeRefreshLayout.setOnRefreshListener {
//            mViewModel.refreshDataSource()
//        }

        // when button was clicked, scrolling list to top.
        fabTop.setOnClickListener {
            mRecyclerView.scrollToPosition(0)
        }

        // menu item clicked event.
        toolbar.setOnMenuItemClickListener {
            onMenuSelected(it)
            true
        }

        // list item clicked event.
        observe(mAdapter.getItemClickEvent(), requireActivity()::jumpBrowser)

        // subscribe UI state
//        observe(mViewModel.viewStateLiveData, this::onNewState)
//        observe(mViewModel.pagedListLiveData, this::onPagedList)
    }

    private fun onPagedList(pagedList: PagedList<Repo>) {
        mAdapter.submitList(pagedList)
        mRecyclerView.scrollToPosition(0)
    }

    private fun onNewState(state: ReposViewState) {
        if (state.throwable != null) {
            // handle throwable
            toast { "network failure." }
        }

//        if (state.isLoading != mSwipeRefreshLayout.isRefreshing) {
//            mSwipeRefreshLayout.isRefreshing = state.isLoading
//        }
    }

    private fun onMenuSelected(menuItem: MenuItem) {
        mViewModel.onSortChanged(
                when (menuItem.itemId) {
                    R.id.menu_repos_letter -> ReposViewModel.sortByLetter
                    R.id.menu_repos_update -> ReposViewModel.sortByUpdate
                    R.id.menu_repos_created -> ReposViewModel.sortByCreated
                    else -> throw IllegalArgumentException("failure menuItem id.")
                }
        )
    }
}