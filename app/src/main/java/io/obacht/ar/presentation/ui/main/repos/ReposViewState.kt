package io.obacht.ar.presentation.ui.main.repos

import androidx.paging.PagedList
import arrow.core.Option
import io.obacht.ar.domain.entity.Repo

data class ReposViewState(
    val isLoading: Boolean,
    val throwable: Option<Throwable>,
    val pagedList: Option<PagedList<Repo>>,
    val nextPageData: Option<List<Repo>>,  // useless in this sample, but it's useful sometimes.
    val sort: String
) {

    companion object {

        fun initial(): ReposViewState {
            return ReposViewState(
                isLoading = false,
                throwable = Option.empty(),
                pagedList = Option.empty(),
                nextPageData = Option.empty(),
                sort = ReposViewModel.sortByUpdate
            )
        }
    }
}