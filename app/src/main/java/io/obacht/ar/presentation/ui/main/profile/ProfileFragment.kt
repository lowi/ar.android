package io.obacht.ar.presentation.ui.main.profile

import android.os.Bundle
import android.view.View
import io.obacht.ar.architecture.core.base.view.fragment.BaseFragment
import io.obacht.ar.architecture.core.ext.observe
import io.obacht.ar.presentation.utils.toast
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment() {


    private val mViewModel: ProfileViewModel by viewModel()

    override val layoutId: Int = io.obacht.ar.image.R.layout.fragment_profile

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binds()
    }

    private fun binds() {
        observe(mViewModel.viewStateLiveData, this::onNewState)

        mBtnEdit.setOnClickListener { toast { "coming soon..." } }
    }

    private fun onNewState(state: ProfileViewState) {
        state.throwable.fold({}) {
            // handle throwable.
        }

        state.userInfo.fold({}) {
//            GlideApp.with(requireContext())
//                    .load(it.avatarUrl)
//                    .apply(RequestOptions().circleCrop())
//                    .into(mIvAvatar)

            mTvNickname.text = it.name
//            mTvBio.text = state.userInfo.bio
//            mTvLocation.text = state.userInfo.location
        }
    }
}