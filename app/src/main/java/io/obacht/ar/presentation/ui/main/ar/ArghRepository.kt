package io.obacht.ar.presentation.ui.main.ar

import android.annotation.SuppressLint
import android.content.res.AssetManager
import arrow.fx.IO
import com.google.ar.core.AugmentedImageDatabase
import com.google.ar.core.Session
import io.obacht.ar.architecture.core.base.repository.BaseRepositoryLocal
import io.obacht.ar.architecture.core.base.repository.ILocalDataSource
import io.obacht.ar.architecture.core.logger.AppLogger
import io.obacht.ar.gl.base.Geometry
import io.obacht.ar.domain.entity.gl.Image
import io.obacht.ar.gl.base.Shader
import io.obacht.ar.domain.entity.gl.Texture
import java.io.IOException
import java.io.InputStream

@SuppressLint("CheckResult")
class ArghRepository(
    private val dataSource: ArDataSource,
) : BaseRepositoryLocal<ArDataSource>(dataSource) {

    fun loadShader(filename: String): IO<Shader> =
        dataSource.loadShader(filename)

    fun loadGeometry(filename: String): IO<Geometry> =
        dataSource.loadGeometry(filename)

    fun loadTexture(filename: String): IO<Texture> =
        dataSource.loadTexture(filename)

    fun loadImage(filename: String): IO<Image> =
        dataSource.loadImage(filename)

    fun loadImageDB(filename: String, session: Session): IO<AugmentedImageDatabase> =
        dataSource.loadImageDB(filename, session)

//    @MainThread
//    fun fetchEventDataSourceFactory(): DataSource.Factory<Int, ReceivedEvent> {
//        return localDataSource.fetchPagedListFromLocal()
//    }

}


@SuppressLint("CheckResult")
class ArDataSource(private val assetManager: AssetManager) : ILocalDataSource {

    fun loadShader(filename: String): IO<Shader> = TODO()
    fun loadGeometry(filename: String): IO<Geometry> = TODO()
    fun loadTexture(filename: String): IO<Texture> = TODO()
    fun loadImage(filename: String): IO<Image> = TODO()

    fun loadImageDB(filename: String, session: Session): IO<AugmentedImageDatabase> =
        loadAsset("images/$filename").flatMap {
            try {
                IO.just(AugmentedImageDatabase.deserialize(session, it))
            } catch (e: IOException) {
                IO.raiseError(io.obacht.ar.domain.ArError.ArCore.ImageDB(e.localizedMessage!!))
            }
        }

    private fun loadAsset(path: String): IO<InputStream> =
        try {
            AppLogger.i("loadAsset: $path", assetManager)
            IO.just(assetManager.open(path))
        } catch (e: IOException) {
            IO.raiseError(io.obacht.ar.domain.ArError.Storage.NotFound(e.localizedMessage!!))
        }
}