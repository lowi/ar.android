package io.obacht.ar.presentation.utils

import io.obacht.ar.architecture.core.ext.toast
import io.obacht.ar.presentation.BaseApplication


inline fun toast(value: () -> String): Unit =
        BaseApplication.INSTANCE.toast(value)