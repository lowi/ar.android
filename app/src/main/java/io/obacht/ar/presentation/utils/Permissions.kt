package io.obacht.ar.presentation.utils

import android.content.Context
import arrow.core.Either
import arrow.fx.IO
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.PermissionRequestErrorListener
import com.karumi.dexter.listener.single.PermissionListener
import io.obacht.ar.domain.ArError

typealias PermissionResponse = Either<String, String>


/**
 * Permissions is a helper object for requesting hardware-permissions (e.g. use of the camera)
 * from the device.
 */
object Permissions {


    /**
     * Request a permission.
     *
     * @param context the android application context
     * @param permission the permission string
     * @return a PermissionResponse wrapped into an IO.
     */
    fun check(context: Context, permission: String): IO<PermissionResponse> =

        IO.async {
            val listener = object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) =
                    it(Either.right(Either.right(permission)))

                override fun onPermissionDenied(response: PermissionDeniedResponse) =
                    it(Either.right(Either.left(permission)))

                override fun onPermissionRationaleShouldBeShown(
                    request: PermissionRequest,
                    token: PermissionToken
                ) = it(Either.left(ArError.PermissionsRationale(request.name)))
            }

            val errorListener = PermissionRequestErrorListener {
                it(Either.left(ArError.Permissions(it.name)))
            }

            Dexter.withContext(context)
                .withPermission(permission)
                .withListener(listener)
                .withErrorListener(errorListener)
                .onSameThread()
                .check()
        }
}