package io.obacht.ar.presentation.ui.main.home

import android.os.Bundle
import android.view.View
import androidx.paging.PagedList
import io.obacht.ar.architecture.core.base.view.fragment.BaseFragment
import io.obacht.ar.architecture.core.ext.observe
import io.obacht.ar.architecture.core.ext.jumpBrowser
import io.obacht.ar.domain.entity.ReceivedEvent
import io.obacht.ar.presentation.utils.removeAllAnimation
import io.obacht.ar.presentation.utils.toast
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment() {

    private val mViewModel: HomeViewModel by viewModel()

    override val layoutId: Int = io.obacht.ar.image.R.layout.fragment_home

    private val mAdapter: HomePagedAdapter = HomePagedAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binds()

        mRecyclerView.adapter = mAdapter
        mRecyclerView.removeAllAnimation()
    }

    private fun binds() {

        // swipe refresh event.
//        mSwipeRefreshLayout.setOnRefreshListener {
//            mViewModel.refreshDataSource()
//        }

        // list item clicked event.
        observe(mAdapter.observeItemEvent(), requireActivity()::jumpBrowser)

        // subscribe UI state.
//        observe(mViewModel.viewStateLiveData, this::onNewState)
//        observe(mViewModel.pagedListLiveData, this::onPagedList)
    }

    private fun onPagedList(pagedList: PagedList<ReceivedEvent>) {
        mAdapter.submitList(pagedList)
        mRecyclerView.scrollToPosition(0)
    }

    private fun onNewState(state: HomeViewState) {
        state.throwable.fold({}) {
            toast {
                it.message ?: "network error."
            }
//            if (state.isLoading != mSwipeRefreshLayout.isRefreshing)
//                mSwipeRefreshLayout.isRefreshing = state.isLoading
        }
    }
}