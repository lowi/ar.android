package io.obacht.ar.presentation.ui.login

import android.os.Bundle
import android.view.View
import android.widget.TextView
import io.obacht.ar.architecture.core.base.view.fragment.BaseFragment
import io.obacht.ar.architecture.core.ext.observe
import io.obacht.ar.presentation.ui.main.MainActivity
import io.obacht.ar.presentation.utils.toast
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment() {

    override val layoutId: Int = io.obacht.ar.image.R.layout.fragment_login

    private val mViewModel: LoginViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binds()
    }

    private fun binds() {
        mBtnSignIn.setOnClickListener {
            mViewModel.login(tvUsername.text.toString(), tvPassword.text.toString())
        }

        observe(mViewModel.stateLiveData, this::onNewState)
    }

    private fun onNewState(state: LoginViewState) {
        state.throwable.fold({}) {
            when (it) {
//                is Errors.EmptyInputError -> "username or password can't be null."
//                is HttpException ->
//                    when (it.code()) {
//                        401 -> "username or password failure."
//                        else -> "network failure"
//                    }
                else -> "network failure"
            }.also { str ->
                toast { str }
            }
        }

        mProgressBar.visibility = if (state.isLoading) View.VISIBLE else View.GONE

        state.autoLoginEvent.fold({}) { autoLoginEvent ->
            if (autoLoginEvent.autoLogin       // allow auto login by user
                && state.useAutoLoginEvent     // ensure auto login info be used one time
            ) {
                tvUsername.setText(autoLoginEvent.username, TextView.BufferType.EDITABLE)
                tvPassword.setText(autoLoginEvent.password, TextView.BufferType.EDITABLE)

                mViewModel.onAutoLoginEventUsed()
                mViewModel.login(autoLoginEvent.username, autoLoginEvent.password)
            }
        }


        state.loginInfo.fold({}) {
            MainActivity.launch(requireActivity())
        }
    }
}