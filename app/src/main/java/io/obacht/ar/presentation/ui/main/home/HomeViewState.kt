package io.obacht.ar.presentation.ui.main.home

import arrow.core.Option

data class HomeViewState(
        val isLoading: Boolean,
        val throwable: Option<Throwable>
) {

    companion object {

        fun initial(): HomeViewState {
            return HomeViewState(
                    isLoading = false,
                    throwable = Option.empty()
            )
        }
    }
}