package io.obacht.ar.presentation.ui.main

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.viewpager.widget.ViewPager
import io.obacht.ar.architecture.core.adapter.ViewPagerAdapter
import io.obacht.ar.architecture.core.base.view.fragment.BaseFragment
import io.obacht.ar.image.R
import io.obacht.ar.presentation.ui.main.ar.ArFragment
import io.obacht.ar.presentation.ui.main.home.HomeFragment
import io.obacht.ar.presentation.ui.main.profile.ProfileFragment
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

@Suppress("PLUGIN_WARNING")
@SuppressLint("CheckResult")
class MainFragment : BaseFragment() {

    override val layoutId: Int = R.layout.fragment_main

    @Suppress("unused")
    private val mViewModel: MainViewModel by viewModel()

    private var isPortMode: Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isPortMode = resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT

        viewPager.adapter = ViewPagerAdapter(childFragmentManager,
                listOf(HomeFragment(), ArFragment(), ProfileFragment()))
        viewPager.offscreenPageLimit = 2

        when (isPortMode) {
            true -> bindsPortScreen()
            false -> bindsLandScreen()
        }
    }

    private fun bindsPortScreen() {
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) = Unit

            override fun onPageSelected(position: Int) = onPageSelectChanged(position)

            override fun onPageScrollStateChanged(state: Int) = Unit
        })

        navigation.setOnNavigationItemSelectedListener { menuItem ->
            onBottomNavigationSelectChanged(menuItem)
            true
        }
    }

    private fun bindsLandScreen() {
        fabHome.setOnClickListener { onPageSelectChanged(0) }
        fabRepo.setOnClickListener { onPageSelectChanged(1) }
        fabProfile.setOnClickListener { onPageSelectChanged(2) }
    }

    private fun onPageSelectChanged(index: Int) {
        // port-mode
        if (isPortMode) {
            for (position in 0..index) {
                if (navigation.visibility == View.VISIBLE)
                    navigation.menu.getItem(position).isChecked = index == position
            }
        } else {
            // land-mode
            if (viewPager.currentItem != index) {
                viewPager.currentItem = index
                if (fabMenu != null && fabMenu.isExpanded)
                    fabMenu.toggle()
            }
        }
    }

    // port-mode only
    private fun onBottomNavigationSelectChanged(menuItem: MenuItem) {
        when (menuItem.itemId) {
            R.id.nav_home -> {
                viewPager.currentItem = 0
            }
            R.id.nav_repos -> {
                viewPager.currentItem = 1
            }
            R.id.nav_profile -> {
                viewPager.currentItem = 2
            }
        }
    }
}