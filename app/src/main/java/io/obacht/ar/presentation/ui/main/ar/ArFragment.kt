package io.obacht.ar.presentation.ui.main.ar

import android.opengl.GLSurfaceView
import android.os.Bundle
import android.view.View
import arrow.core.Option
import com.google.ar.core.Session
import io.obacht.ar.architecture.core.base.view.fragment.BaseFragment
import io.obacht.ar.architecture.core.ext.observe
import io.obacht.ar.architecture.core.logger.AppLogger
import io.obacht.ar.image.R
import io.obacht.ar.presentation.utils.GlAssets
import io.obacht.ar.presentation.utils.toast
import org.koin.androidx.viewmodel.ext.android.viewModel

class ArFragment : BaseFragment() {


    override val layoutId: Int = R.layout.fragment_ar

    private val viewModel: ArViewModel by viewModel()

    private lateinit var surfaceView: GLSurfaceView

    // local state received from the ViewState after the viewmodel has initialized
    private var arSession: Option<Session> = Option.empty()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        surfaceView = view.findViewById(R.id.surfaceView)
        initialize()
    }


    override fun onResume() {
        super.onResume()
        AppLogger.i("onResume $arSession")
        arSession.fold(
            ifEmpty = { /*initialize()*/ },
            ifSome = {
                // obacht: order is important
                it.resume()
                surfaceView.onResume()
            })
    }


    override fun onPause() {
        super.onPause()
        arSession.fold({}) {
            // obacht: order is important
            surfaceView.onPause()
            it.pause()
        }
    }

    private fun initialize() {
        viewModel.initialize(requireActivity(), surfaceView, GlAssets.load())
        observe(viewModel.viewStateLiveData, this::onNewState)
    }


    private fun onNewState(state: ArViewState) {
        state.throwable.fold({}) {
            toast { it.message ?: "wat?" }
        }

        state.arSession.fold({}) {
            arSession = Option.just(it)
        }

    }
}