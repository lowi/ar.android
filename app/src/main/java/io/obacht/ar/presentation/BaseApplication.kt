package io.obacht.ar.presentation

import android.app.Application
import io.obacht.ar.architecture.core.logger.CrashReportingTree
import io.obacht.ar.data.di.dataModule
import io.obacht.ar.gl.di.glModule
import io.obacht.ar.image.BuildConfig
import io.obacht.ar.presentation.di.appModule
import io.obacht.ar.presentation.di.loginModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber


open class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        loadKoin()
        initLogger()
    }

    private fun loadKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@BaseApplication)
//            androidFileProperties()
            modules(dataModule)
            modules(glModule)
            modules(loginModule)
            modules(appModule)
        }
    }


    private fun initLogger() {
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
        else
            Timber.plant(CrashReportingTree())
    }

    companion object {
        lateinit var INSTANCE: BaseApplication
    }
}