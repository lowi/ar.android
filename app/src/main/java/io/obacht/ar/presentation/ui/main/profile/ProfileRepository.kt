package io.obacht.ar.presentation.ui.main.profile

import io.obacht.ar.architecture.core.base.repository.BaseRepositoryRemote
import io.obacht.ar.architecture.core.base.repository.IRemoteDataSource


interface IRemoteProfileDataSource : IRemoteDataSource

class ProfileRepository(
        remoteDataSource: IRemoteProfileDataSource
) : BaseRepositoryRemote<IRemoteProfileDataSource>(remoteDataSource)

class ProfileRemoteDataSource(
//        val serviceManager: ServiceManager
) : IRemoteProfileDataSource