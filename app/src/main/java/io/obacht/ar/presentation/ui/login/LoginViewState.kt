package io.obacht.ar.presentation.ui.login

import arrow.core.Option
import io.obacht.ar.domain.entity.UserInfo


data class LoginViewState(
    val isLoading: Boolean,
    val throwable: Option<Throwable>,
    val loginInfo: Option<UserInfo>,
    val autoLoginEvent: Option<AutoLoginEvent>,
    val useAutoLoginEvent: Boolean      // the flag ensure login info display one time.
) {

    companion object {

        fun initial(): LoginViewState {
            return LoginViewState(
                    isLoading = false,
                    throwable = Option.empty(),
                    loginInfo = Option.empty(),
                    autoLoginEvent = Option.empty(),
                    useAutoLoginEvent = true
            )
        }
    }

}