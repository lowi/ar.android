package io.obacht.ar.presentation.ui.main.repos

import android.annotation.SuppressLint
import androidx.annotation.AnyThread
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.paging.DataSource
import io.obacht.ar.architecture.core.base.repository.BaseRepositoryBoth
import io.obacht.ar.architecture.core.base.repository.ILocalDataSource
import io.obacht.ar.architecture.core.base.repository.IRemoteDataSource
import io.obacht.ar.domain.db.UserDatabase
import io.obacht.ar.domain.entity.Repo
import io.obacht.ar.presentation.base.Results
import io.obacht.ar.presentation.manager.UserManager
import io.obacht.ar.presentation.utils.PAGING_REMOTE_PAGE_SIZE

@SuppressLint("CheckResult")
class ReposRepository(
    remote: RemoteReposDataSource,
    local: LocalReposDataSource
) : BaseRepositoryBoth<RemoteReposDataSource, LocalReposDataSource>(remote, local) {

    @MainThread
    suspend fun fetchRepoByPage(
        sort: String,
        pageIndex: Int,
        remoteRequestPerPage: Int = PAGING_REMOTE_PAGE_SIZE
    ): Results<List<Repo>> {
        val username: String = UserManager.INSTANCE.login
        return remoteDataSource.queryRepos(username, pageIndex, remoteRequestPerPage, sort)
    }

    @MainThread
    fun fetchRepoDataSourceFactory(): DataSource.Factory<Int, Repo> {
        return localDataSource.fetchRepoDataSourceFactory()
    }

    @WorkerThread
    suspend fun clearAndInsertNewData(items: List<Repo>) {
        localDataSource.clearOldAndInsertNewData(items)
    }

    @WorkerThread
    suspend fun insertNewPageData(items: List<Repo>) {
        localDataSource.insertNewPageData(items)
    }
}

class RemoteReposDataSource(
    // private val serviceManager: ServiceManager
) : IRemoteDataSource {

    suspend fun queryRepos(
        username: String,
        pageIndex: Int,
        perPage: Int,
        sort: String
    ): Results<List<Repo>> {
        return fetchReposByPageInternal(username, pageIndex, perPage, sort)
    }

    private suspend fun fetchReposByPageInternal(
        username: String,
        pageIndex: Int,
        perPage: Int,
        sort: String
    ): Results<List<Repo>> {
//        val response = serviceManager.userService.queryRepos(username, pageIndex, perPage, sort)
//        return processApiResponse(response)
        TODO()
    }
}

class LocalReposDataSource(
    private val db: UserDatabase
) : ILocalDataSource {

    @AnyThread
    fun fetchRepoDataSourceFactory(): DataSource.Factory<Int, Repo> {
        return db.userReposDao().queryRepos()
    }

    @AnyThread
    suspend fun clearOldAndInsertNewData(newPage: List<Repo>) {
//        db.withTransaction {
//            db.userReposDao().deleteAllRepos()
//            insertDataInternal(newPage)
//        }
        TODO()
    }

    @AnyThread
    suspend fun insertNewPageData(newPage: List<Repo>) {
//        db.withTransaction { insertDataInternal(newPage) }
        TODO()
    }

    @AnyThread
    private suspend fun insertDataInternal(newPage: List<Repo>) {
//        val start = db.userReposDao().getNextIndexInRepos() ?: 0
//        val items = newPage.mapIndexed { index, child ->
//            child.indexInSortResponse = start + index
//            child
//        }
//        db.userReposDao().insert(items)
        TODO()
    }
}