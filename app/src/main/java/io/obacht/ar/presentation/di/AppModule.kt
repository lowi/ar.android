package io.obacht.ar.presentation.di

import android.content.Context
import android.content.SharedPreferences
import io.obacht.ar.domain.repository.UserInfoRepository
import io.obacht.ar.presentation.BaseApplication
import io.obacht.ar.presentation.ui.main.MainViewModel
import io.obacht.ar.presentation.ui.main.ar.ArViewModel
import io.obacht.ar.presentation.ui.main.home.HomeLocalDataSource
import io.obacht.ar.presentation.ui.main.home.HomeRemoteDataSource
import io.obacht.ar.presentation.ui.main.home.HomeRepository
import io.obacht.ar.presentation.ui.main.home.HomeViewModel
import io.obacht.ar.presentation.ui.main.profile.IRemoteProfileDataSource
import io.obacht.ar.presentation.ui.main.profile.ProfileRemoteDataSource
import io.obacht.ar.presentation.ui.main.profile.ProfileRepository
import io.obacht.ar.presentation.ui.main.profile.ProfileViewModel
import io.obacht.ar.presentation.ui.main.repos.LocalReposDataSource
import io.obacht.ar.presentation.ui.main.repos.RemoteReposDataSource
import io.obacht.ar.presentation.ui.main.repos.ReposRepository
import io.obacht.ar.presentation.ui.main.repos.ReposViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


private const val DEFAULT_SP_TAG = "PrefsDefault"


val appModule = module {

    // this is stupid an a remnant of the seed project
    // todo: clean up / remove
    single {
        HomeRemoteDataSource()
    }
    single {
        HomeLocalDataSource(db = get())
    }
    single {
        HomeRepository(remoteDataSource = get(), localDataSource = get())
    }
    single<IRemoteProfileDataSource> {
        ProfileRemoteDataSource()
    }
    single {
        ProfileRepository(remoteDataSource = get())
    }
    single {
        ReposViewModel(repository = get())
    }
    single {
        ReposRepository(
            remote = RemoteReposDataSource(/*serviceManager = instance()*/),
            local = LocalReposDataSource(db = get())
        )
    }


    single<SharedPreferences> {
        BaseApplication.INSTANCE.getSharedPreferences(DEFAULT_SP_TAG, Context.MODE_PRIVATE)
    }

    single {
        UserInfoRepository.getInstance(get())
    }


    // Viewmodels
    // ——————————

    viewModel {
        MainViewModel()
    }

    viewModel {
        HomeViewModel(get())
    }

    viewModel {
        ArViewModel(glRenderer = get(), repository = get())
    }

    viewModel {
        ProfileViewModel(repo = get())
    }



//    bind<BottomNavigationView>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
//        (context as MainFragment).navigation
//    }
//
//    bind<ViewPager>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
//        (context as MainFragment).viewPager
//    }
//
//    bind<List<Fragment>>(MAIN_LIST_FRAGMENT) with scoped<Fragment>(AndroidLifecycleScope).singleton {
//        listOf<Fragment>(
//            instance<HomeFragment>(),
//            instance<ReposFragment>(),
//            instance<ProfileFragment>()
//        )
//    }
}



