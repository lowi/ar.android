package io.obacht.ar.presentation.ui.main.ar

import arrow.core.Option
import com.google.ar.core.Session


data class ArViewState(
    val isLoading: Boolean,
    val throwable: Option<Throwable>,

    val apkInstallRequested: Boolean,
    val arSession: Option<Session>
) {

    companion object {

        fun initial(): ArViewState {
            return ArViewState(
                isLoading = false,
                throwable = Option.empty(),

                apkInstallRequested = false,
                arSession = Option.empty()
            )
        }
    }
}