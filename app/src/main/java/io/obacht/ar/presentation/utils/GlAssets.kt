package io.obacht.ar.presentation.utils

import io.obacht.ar.domain.entity.gl.AssetId
import io.obacht.ar.domain.entity.gl.GlFile
import io.obacht.ar.domain.entity.gl.GlType

object GlAssets {

    fun load(): Map<AssetId, GlFile<GlType>> {
        val glAssets = HashMap<AssetId, GlFile<GlType>>()

        glAssets["shader_camera_vertex"] = GlFile("screenquad.vert", GlType.Shader.Vertex)
        glAssets["shader_camera_fragment"] = GlFile("screenquad.frag", GlType.Shader.Fragment)

        glAssets["shader_box_vertex"] = GlFile("cube.vert", GlType.Shader.Vertex)
        glAssets["shader_box_fragment"] = GlFile("cube.frag", GlType.Shader.Fragment)

        glAssets["shader_stencil_fragment"] = GlFile("stencil.frag", GlType.Shader.Fragment)
        glAssets["shader_stencil_vertex"] = GlFile("stencil.vert", GlType.Shader.Vertex)

        glAssets["geometry_box"] = GlFile("open_cube.obj", GlType.Geometry)
        glAssets["geometry_stencil"] = GlFile("plane.obj", GlType.Geometry)

        return glAssets

    }
}