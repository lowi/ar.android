package io.obacht.ar.presentation.ui.login

import io.obacht.ar.architecture.core.base.repository.BaseRepositoryBoth
import io.obacht.ar.architecture.core.base.repository.ILocalDataSource
import io.obacht.ar.architecture.core.base.repository.IRemoteDataSource
import io.obacht.ar.domain.entity.UserInfo
import io.obacht.ar.presentation.base.Results

class LoginRepository(
    remoteDataSource: LoginRemoteDataSource,
    localDataSource: LoginLocalDataSource
) : BaseRepositoryBoth<LoginRemoteDataSource, LoginLocalDataSource>(
    remoteDataSource,
    localDataSource
) {

    suspend fun login(username: String, password: String): Results<UserInfo> {
        // 保存用户登录信息
        localDataSource.savePrefUser(username, password)
        val userInfo = remoteDataSource.login()

//        // 如果登录失败，清除登录信息
//        when (userInfo) {
//            is Results.Failure -> localDataSource.clearPrefsUser()
//            is Results.Success -> UserManager.INSTANCE = requireNotNull(userInfo.data)
//        }

        return userInfo
    }

    fun fetchAutoLogin(): AutoLoginEvent {
        return localDataSource.fetchAutoLogin()
    }
}

class LoginRemoteDataSource(
//        private val serviceManager: ServiceManager
) : IRemoteDataSource {

    suspend fun login(): Results<UserInfo> {
        // 1.用户登录认证
//        val userAccessTokenResponse =
//                serviceManager.loginService.authorizations(LoginRequestModel.generate())
//        val results1 = processApiResponse(userAccessTokenResponse)
//        return when (results1) {
//            is Results.Success -> {
//                // 2.获取用户详细信息
//                val userInfoResponse = serviceManager.userService.fetchUserOwner()
//                processApiResponse(userInfoResponse)
//            }
//            is Results.Failure -> results1
//        }
        TODO()
    }
}

class LoginLocalDataSource(
//    private val db: UserDatabase,
//    private val userRepository: io.obacht.ar.domain.repository.UserInfoRepository
) : ILocalDataSource {

    fun savePrefUser(username: String, password: String) {
//        userRepository.username = username
//        userRepository.password = password
    }

    fun clearPrefsUser() {
//        userRepository.username = ""
//        userRepository.password = ""
    }

    fun fetchAutoLogin(): AutoLoginEvent {
//        val username = userRepository.username
//        val password = userRepository.password
//        val isAutoLogin = userRepository.isAutoLogin
//        return when (username.isNotEmpty() && password.isNotEmpty() && isAutoLogin) {
//            true -> AutoLoginEvent(true, username, password)
//            false -> AutoLoginEvent(false, "", "")
//        }
        TODO()
    }
}

data class AutoLoginEvent(
    val autoLogin: Boolean,
    val username: String,
    val password: String
)