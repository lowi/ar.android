package io.obacht.ar.presentation.di

import android.content.Context
import android.content.SharedPreferences
import androidx.fragment.app.Fragment
import io.obacht.ar.data.MockUserDatabase
import io.obacht.ar.domain.db.UserDatabase
import io.obacht.ar.domain.repository.UserInfoRepository
import io.obacht.ar.presentation.BaseApplication
import io.obacht.ar.presentation.ui.login.*
import io.obacht.ar.presentation.ui.main.ar.ArViewModel
import io.obacht.ar.presentation.ui.main.home.HomeLocalDataSource
import io.obacht.ar.presentation.ui.main.home.HomeRemoteDataSource
import io.obacht.ar.presentation.ui.main.home.HomeRepository
import io.obacht.ar.presentation.ui.main.home.HomeViewModel
import io.obacht.ar.presentation.ui.main.profile.ProfileRemoteDataSource
import io.obacht.ar.presentation.ui.main.profile.ProfileRepository
import io.obacht.ar.presentation.ui.main.profile.ProfileViewModel
import io.obacht.ar.presentation.ui.main.repos.LocalReposDataSource
import io.obacht.ar.presentation.ui.main.repos.RemoteReposDataSource
import io.obacht.ar.presentation.ui.main.repos.ReposRepository
import io.obacht.ar.presentation.ui.main.repos.ReposViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val loginModule = module {

    single {
        LoginRemoteDataSource()
    }
    single {
        LoginLocalDataSource()
    }
    single {
        LoginRepository(get(), get())
    }

    viewModel {
        LoginViewModel(repo = get())
    }
}



