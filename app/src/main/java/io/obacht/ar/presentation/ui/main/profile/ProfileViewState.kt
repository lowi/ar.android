package io.obacht.ar.presentation.ui.main.profile

import arrow.core.Option
import io.obacht.ar.domain.entity.UserInfo

data class ProfileViewState(
        val isLoading: Boolean,
        val throwable: Option<Throwable>,
        val userInfo: Option<UserInfo>
) {

    companion object {

        fun initial(): ProfileViewState {
            return ProfileViewState(
                    isLoading = false,
                    throwable = Option.empty(),
                    userInfo = Option.empty()
            )
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ProfileViewState

        if (isLoading != other.isLoading) return false
        if (throwable != other.throwable) return false
        if (userInfo != other.userInfo) return false

        return true
    }

    override fun hashCode(): Int {
        var result = isLoading.hashCode()
        result = 31 * result + throwable.hashCode()
        result = 31 * result + userInfo.hashCode()
        return result
    }

}