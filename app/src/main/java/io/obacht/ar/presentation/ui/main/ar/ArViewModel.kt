package io.obacht.ar.presentation.ui.main.ar

import android.Manifest
import android.app.Activity
import android.content.Context
import android.opengl.GLSurfaceView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import arrow.core.Option
import arrow.fx.IO
import arrow.fx.extensions.fx
import io.obacht.ar.architecture.core.base.viewmodel.BaseViewModel
import io.obacht.ar.architecture.core.ext.setNext
import io.obacht.ar.architecture.core.logger.AppLogger
import io.obacht.ar.domain.ArError
import io.obacht.ar.domain.entity.gl.GlAsset
import io.obacht.ar.domain.entity.gl.GlType
import io.obacht.ar.domain.entity.gl.GlFile
import io.obacht.ar.domain.entity.gl.AssetId
import io.obacht.ar.domain.repository.ArRepository
import io.obacht.ar.gl.GlRenderer
import io.obacht.ar.gl.utils.DisplayRotationHelper
import io.obacht.ar.gl.utils.TrackingStateHelper
import io.obacht.ar.gl.utils.configure
import io.obacht.ar.presentation.utils.ArCoreManager
import io.obacht.ar.presentation.utils.ArInstallResponse
import io.obacht.ar.presentation.utils.Permissions
import io.obacht.ar.presentation.utils.toast
import com.google.ar.core.Session as ArSession

@SuppressWarnings("checkResult")
class ArViewModel(
    private val glRenderer: GlRenderer,
    private val repository: ArRepository
) : BaseViewModel() {

    private val _viewStateLiveData: MutableLiveData<ArViewState> =
        MutableLiveData(ArViewState.initial())

    // expose an immutable version ov the live data
    val viewStateLiveData: LiveData<ArViewState> = _viewStateLiveData


    // functions for receiving messages from GL
    private fun handleGlMessage(msg: String) {
    }

    // functions for receiving errors thrown by GL
    //    private fun handleGlError(err: Throwable) {}


    /**
     * Initialize the openGl/arCore system
     *
     * Loads the augmented-images database.
     * Configures the arSession
     * Loads all assets (shaders, textures, etc.)
     * probably more to come
     *
     * @param activity The android activity/context in which we're running
     * @param surfaceView The gl surface to draw on
     */
    fun initialize(
        activity: Activity,
        surfaceView: GLSurfaceView,
        glAssets: Map<AssetId, GlFile<GlType>>
    ) =
        // make sure we're allowed to use the camera
        checkCameraPermissions(activity)

            // make sure the arCore apk is installed
            .flatMap { installApk(activity) }

            // create an ar session
            .flatMap { makeArSession(activity) }

            .flatMap { session ->
                IO.fx {
                    // load the assets from the filesystem
                    val assets = loadAssets(repository, glAssets).bind()
                    initializeRenderer(activity, surfaceView, session, assets).bind()
                    _viewStateLiveData.setNext { last -> last.copy(arSession = Option.just(session)) }
                }
            }
            .unsafeRunAsync { maybe ->
                maybe.fold({
                    // oh noes! something went wrong
                    AppLogger.e(it.localizedMessage!!)
                    toast { it.localizedMessage!! }
                }) {
                    AppLogger.i("gl initialized successfully")
                    // if all is well, nothing to do
                }
            }

    private fun loadAssets(
        repository: ArRepository,
        glAssets: Map<AssetId, GlFile<GlType>>
    ): IO<Map<AssetId, GlAsset<GlType>>> = repository.loadAssets(glAssets)


    private fun initializeRenderer(
        activity: Activity,
        surfaceView: GLSurfaceView,
        session: ArSession,
        assets: Map<AssetId, GlAsset<GlType>>
    ): IO<Unit> =
        IO.fx {
            AppLogger.i("initializeRenderer", surfaceView)
            glRenderer.initialize(
                activity_ = activity,
                session_ = session,
                surfaceView_ = surfaceView,
                assets_ = assets,
                trackingStateHelper_ = TrackingStateHelper(activity),
                displayRotationHelper_ = DisplayRotationHelper(activity),
                messageFn_ = ::handleGlMessage
            ).bind()
        }


    private fun makeArSession(activity: Activity): IO<ArSession> =
        IO.fx {
            val session = ArSession(activity)
            session.configure(repository).bind()
        }


    private fun checkCameraPermissions(context: Context): IO<Unit> =
        Permissions.check(context, Manifest.permission.CAMERA)
            .map { permission ->
                permission.fold({
                    // not good. permission was not granted.
                    toast { "permission denied: $it" }
                    IO.raiseError<Unit>(ArError.Camera.Permission)
                }) {
                    IO.unit
                }
            }

    private fun installApk(activity: Activity): IO<Unit> {
        val installRequested = _viewStateLiveData.value!!.apkInstallRequested
        return ArCoreManager.installArCore(activity, installRequested)
            .map {
                when (it) {
                    ArInstallResponse.INSTALL_REQUESTED -> {
                        // obacht: side-effect
                        _viewStateLiveData.setNext { last -> last.copy(apkInstallRequested = true) }
                        IO.raiseError(ArError.ArCore.Apk.InstallationRequested)
                    }
                    ArInstallResponse.INSTALLED -> IO.unit
                }
            }
    }
}