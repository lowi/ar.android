package io.obacht.ar.presentation.ui.login

import androidx.lifecycle.*
import arrow.core.Option
import io.obacht.ar.architecture.core.base.viewmodel.BaseViewModel
import io.obacht.ar.architecture.core.ext.postNext
import io.obacht.ar.domain.ArError
import io.obacht.ar.domain.entity.UserInfo
import io.obacht.ar.presentation.base.Results
import kotlinx.coroutines.launch

@SuppressWarnings("checkResult")
class LoginViewModel(
    private val repo: LoginRepository
) : BaseViewModel() {

    private val _stateLiveData: MutableLiveData<LoginViewState> =
        MutableLiveData(LoginViewState.initial())

    val stateLiveData: LiveData<LoginViewState> = _stateLiveData

    init {
        viewModelScope.launch {
            val autoLoginEvent = repo.fetchAutoLogin()
            _stateLiveData.postNext { state ->
                state.copy(
                    isLoading = false,
                    throwable = Option.empty(),
                    autoLoginEvent = Option.just(autoLoginEvent),
                    useAutoLoginEvent = true,
                    loginInfo = Option.empty()
                )
            }
        }
    }

    fun onAutoLoginEventUsed() {
        _stateLiveData.postNext { state ->
            state.copy(
                isLoading = false,
                throwable = Option.empty(),
                autoLoginEvent = Option.empty(),
                useAutoLoginEvent = false,
                loginInfo = Option.empty()
            )
        }
    }

    fun login(username: String?, password: String?) {
        when (username.isNullOrEmpty() || password.isNullOrEmpty()) {
            true -> _stateLiveData.postNext { state ->
                state.copy(
                    isLoading = false, throwable = Option.just(ArError.EmptyInputError),
                    loginInfo = Option.empty(), autoLoginEvent = Option.empty()
                )
            }
            false -> viewModelScope.launch {
                _stateLiveData.postNext {
                    it.copy(
                        isLoading = true,
                        throwable = Option.empty(),
                        loginInfo = Option.empty(),
                        autoLoginEvent = Option.empty()
                    )
                }
                when (val result = repo.login(username, password)) {
                    is Results.Failure -> _stateLiveData.postNext {
                        it.copy(
                            isLoading = false,
                            throwable = Option.just(result.error),
                            loginInfo = Option.empty(),
                            autoLoginEvent = Option.empty()
                        )
                    }
                    is Results.Success<*> -> _stateLiveData.postNext {
                        it.copy(
                            isLoading = false,
                            throwable = Option.empty(),
                            loginInfo = Option.just(result.data as UserInfo),
                            autoLoginEvent = Option.empty()
                        )
                    }
                }
            }
        }
    }
}

@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory(
    private val repo: LoginRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        LoginViewModel(repo) as T
}