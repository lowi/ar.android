#version 300 es
precision mediump float;

out vec4 fragmentColor;

void main() {
    fragmentColor.rgb = vec3(1.0, 1.0, 1.0);
    fragmentColor.a = 1.0;
}