#version 300 es

uniform mat4 u_ModelView;
uniform mat4 u_ModelViewProjection;

in vec3 a_Normal;
in vec4 a_Position;

out vec3 v_ViewPosition;
out vec3 v_Normal;
out vec3 v_ViewNormal;
out vec3 v_ScreenSpacePosition;

void main() {
    v_ViewPosition = (u_ModelView * a_Position).xyz;
    v_ViewNormal = normalize((u_ModelView * vec4(a_Normal, 0.0)).xyz);
    v_Normal = a_Normal;
    gl_Position = u_ModelViewProjection * a_Position;
    v_ScreenSpacePosition = gl_Position.xyz / gl_Position.w;
}