package io.obacht.ar.data

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.android.material.internal.ContextUtils.getActivity
import com.google.ar.core.Session
import io.obacht.ar.domain.entity.gl.*
import io.obacht.ar.domain.repository.ArRepository
import org.junit.After

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ArRepositoryTest {

    private lateinit var repository: ArRepository

    @Before
    fun init() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        val assetManager = appContext.assets
        repository = FilesystemArRepository(assetManager)
    }

    @After
    fun close() {
        println("AR_TEST close")
    }

    @Test
    fun loading_fragment_shader_should_succeed() {

        val file = GlFile("object.frag", GlType.Shader.Fragment)
        val result = repository
            .loadAsset(file)
            .unsafeRunSync()

        assertTrue(result is ShaderSource<*>)

        val ss: ShaderSource<GlType.Shader> = result as ShaderSource<GlType.Shader>

        assertTrue(ss.shaderType is GlType.Shader.Fragment)
    }

    @Test
    fun loading_vertex_shader_should_succeed() {

        val file = GlFile("object.vert", GlType.Shader.Vertex)
        val result = repository
            .loadAsset(file)
            .unsafeRunSync()

        assertTrue(result is ShaderSource<*>)

        val ss: ShaderSource<GlType.Shader> = result as ShaderSource<GlType.Shader>

        assertTrue(ss.shaderType is GlType.Shader.Vertex)
    }

    @Test
    fun loading_obj_geometry_should_succeed() {
        val file = GlFile("open_cube.obj", GlType.Geometry)
        val result = repository
            .loadAsset(file)
            .unsafeRunSync()
        assertTrue(result is ObjGeometry)
    }

    @Test
    fun loading_an_asset_map_should_succeed() {

        val glAssets = HashMap<AssetId, GlFile<GlType>>()
        glAssets["box_vertex"] = GlFile("object.vert", GlType.Shader.Vertex)
        glAssets["box_fragment"] = GlFile("object.frag", GlType.Shader.Fragment)

        val result = repository.loadAssets(glAssets).unsafeRunSync()

        assertEquals(result.size, 2)

    }
}