package io.obacht.ar.data.di

import io.obacht.ar.data.FilesystemArRepository
import io.obacht.ar.data.MockUserDatabase
import io.obacht.ar.domain.db.UserDatabase
import io.obacht.ar.domain.repository.ArRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


val dataModule = module {

    single<ArRepository> {
        FilesystemArRepository(assetManager = androidContext().assets)
    }

    single<UserDatabase> {
        MockUserDatabase()
    }

}



