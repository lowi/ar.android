package io.obacht.ar.data

import android.content.res.AssetManager
import arrow.fx.IO
import arrow.fx.extensions.fx
import com.google.ar.core.AugmentedImageDatabase
import com.google.ar.core.Session
import de.javagl.obj.Obj
import de.javagl.obj.ObjData
import de.javagl.obj.ObjReader
import de.javagl.obj.ObjUtils
import io.obacht.ar.architecture.core.base.repository.ILocalDataSource
import io.obacht.ar.architecture.core.util.SingletonHolderSingleArg
import io.obacht.ar.domain.ArError
import io.obacht.ar.domain.entity.gl.*
import io.obacht.ar.domain.repository.ArRepository
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.nio.*

class FilesystemArRepository(
    private val assetManager: AssetManager
) : ArRepository, ILocalDataSource {


    override fun <T : GlType> loadAsset(glFile: GlFile<T>): IO<GlAsset<T>> =
        openInputStream(assetPath(glFile))
            .flatMap {
                load(it, glFile.type) as IO<GlAsset<T>>
            }


    override fun loadImageDB(filename: String, session: Session): IO<AugmentedImageDatabase> =
        openInputStream("images/$filename").flatMap {
            try {
                IO.just(AugmentedImageDatabase.deserialize(session, it))
            } catch (e: IOException) {
                IO.raiseError(ArError.ArCore.ImageDB(e.localizedMessage!!))
            }
        }



    private fun assetPath(file: GlFile<*>): String =
        when (file.type) {
            is GlType.Shader -> "$SHADER_PREFIX${file.name}"
            is GlType.Texture -> "$TEXTURE_PREFIX${file.name}"
            is GlType.Geometry -> "$GEOMETRY_PREFIX${file.name}"
        }


    private fun load(inputStream: InputStream, type: GlType): IO<GlAsset<GlType>> =
        when (type) {
            is GlType.Shader -> loadShader(inputStream, type) as IO<GlAsset<GlType>>
            is GlType.Texture -> TODO()
            is GlType.Geometry -> parseObj(inputStream) as IO<GlAsset<GlType>>
            else -> IO.raiseError(ArError.Gl.Asset.Type(type))
        }


    private fun <S : GlType.Shader> loadShader(inputStream: InputStream, type: S): IO<GlAsset<S>> =
        stringFromAssetFile(inputStream)
            .map { ShaderSource(it, type) as GlAsset<S> }



//    override fun loadShader(filename: String): IO<ShaderSource> = TODO()
//        IO.fx {
//            loadAsset("$SHADER_PREFIX$filename")
//                .flatMap { stringFromAssetFile(it) }
//                .map {
//                    when {
//                        vertexRe.matches(filename) -> ShaderSource.Vertex(it)
//                        fragmentRe.matches(filename) -> ShaderSource.Fragment(it)
//                        geometryRe.matches(filename) -> ShaderSource.Geometry(it)
//
//                        // todo: handle error correctly
//                        else -> throw RuntimeException("unknown shader suffix $filename")
//                    }
//                }
//                .bind()
//        }


    private fun openInputStream(path: String): IO<InputStream> =
        try {
            IO.just(assetManager.open(path))
        } catch (e: IOException) {
            IO.raiseError(ArError.Storage.NotFound(e.localizedMessage!!))
        }

    private fun stringFromAssetFile(stream: InputStream): IO<String> =
        try {
            val shaderSource = StringBuilder()
            val reader = BufferedReader(InputStreamReader(stream))
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                shaderSource.append(line).append("\n")
            }
            reader.close()
            IO.just(shaderSource.toString())
        } catch (e: IOException) {
            IO.raiseError(ArError.Shader.Load)
        }


    private fun parseObj(inputStream: InputStream): IO<ObjGeometry> =
        IO.fx {
            var obj: Obj = ObjReader.read(inputStream)

            // Prepare the Obj so that its structure is suitable for
            // rendering with OpenGL:
            // 1. Triangulate it
            // 2. Make sure that texture coordinates are not ambiguous
            // 3. Make sure that normals are not ambiguous
            // 4. Convert it to single-indexed data
            obj = ObjUtils.convertToRenderable(obj)

            // OpenGL does not use Java arrays. ByteBuffers are used instead to provide data in a format
            // that OpenGL understands.
            // Obtain the data from the OBJ, as direct buffers:
            val indices: ShortBuffer = loadObjIndices(obj)
            val vertices: FloatBuffer = ObjData.getVertices(obj)
            val textureCoordinates: FloatBuffer = ObjData.getTexCoords(obj, 2)
            val normals: FloatBuffer = ObjData.getNormals(obj)

            ObjGeometry(indices, vertices, textureCoordinates, normals)
        }


    // Load int indices as shorts for GL ES 2.0 compatibility
    private fun loadObjIndices(obj: Obj): ShortBuffer {
        val wideIndices: IntBuffer = ObjData.getFaceVertexIndices(obj, 3)
        val indices = ByteBuffer.allocateDirect(2 * wideIndices.limit())
            .order(ByteOrder.nativeOrder())
            .asShortBuffer()
        while (wideIndices.hasRemaining()) {
            indices.put(wideIndices.get().toShort())
        }
        indices.rewind()
        return indices
    }


    companion object :
        SingletonHolderSingleArg<FilesystemArRepository, AssetManager>(::FilesystemArRepository) {
        private val fragmentRe = Regex(".*\\.frag")
        private val vertexRe = Regex(".*\\.vert")
        private val geometryRe = Regex(".*\\.geo")

    }

}