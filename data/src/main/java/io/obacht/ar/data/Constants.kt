package io.obacht.ar.data


// path constants for retrieving shaders from the assets
internal const val SHADER_PREFIX = "shaders/"
internal const val TEXTURE_PREFIX = "textures/"
internal const val GEOMETRY_PREFIX = "geometry/"

internal const val VERTEX_SHADER_SUFFIX = ".vert"
internal const val FRAGMENT_SHADER_SUFFIX = ".frag"