package io.obacht.ar.data

import io.obacht.ar.domain.db.UserDatabase
import io.obacht.ar.domain.db.UserReceivedEventDao
import io.obacht.ar.domain.db.UserReposDao

class MockUserDatabase : UserDatabase() {
    override fun userReceivedEventDao(): UserReceivedEventDao {
        TODO("Not yet implemented")
    }

    override fun userReposDao(): UserReposDao {
        TODO("Not yet implemented")
    }
}